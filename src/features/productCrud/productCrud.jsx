import React, { useState } from "react";
import _ from "lodash";
import { ProductTable } from "./product__table";
import { ProductSearch } from "./product__search";
import { ProductForm } from "./product__form";
import { useModal } from "core/hooks/modalHook";
import { toast } from "react-toastify";
import { deleteProduct } from "./productCrudService";
export const ProductCrud = () => {
  const [productTypes, setproductTypes] = useState([]);
  const [action, setAction] = useState("");
  const [selectedInput, setSelectedInput] = useState();
  const [selectedId, setSelectedId] = useState(0);
  const handleSearchResults = (output) => {
    // console.log(output);
    setproductTypes(output);
  };

  // Search Options

  // Add Action
  const handleAddNew = () => {
    setAction("ADD");
    setSelectedId(0);
    setSelectedInput({});
    openFormModal();
  };

  // Edit Action
  const handleEdit = (data) => {
    setAction("EDIT");
    setSelectedId(data["id"]);
    setSelectedInput(data);
    openFormModal();
  };

  // Delete Action
  const handleDelete = async (data) => {
    try {
      setAction("DELETE");
      setSelectedId(data["id"]);
      setSelectedInput(data);
      const { data: product } = await deleteProduct(data["id"]);
      toast.success("product deleted successfully");
      const newList = [...productTypes];
      _.remove(newList, function (n) {
        console.log("Checking " + data["id"] + " " + n["id"]);
        return n["id"] == data["id"];
      });
      // console.log(newList);
      setproductTypes(newList);
    } catch (error) {
      console.log(error);
      toast.error("Error deleting product");
    }
  };

  // Update on success
  const handleAddUpdateSuccess = (updatedData) => {
    if (action === "EDIT") {
      const index = _.findIndex(productTypes, { id: selectedId });
      const updatedDataList = [...productTypes];
      updatedDataList[index] = updatedData;
      setproductTypes(updatedDataList);
    } else if (action === "ADD") {
      const newList = [...productTypes];
      newList.push(updatedData);
      setproductTypes(newList);
    }
  };

  const [formModal, openFormModal, closeFormModal] = useModal(false);

  return (
    <React.Fragment>
      <ProductSearch
        onSearchResults={handleSearchResults}
        onAddNew={handleAddNew}
      />
      <ProductTable
        data={productTypes}
        onEdit={handleEdit}
        onDelete={handleDelete}
      />
      <ProductForm
        open={formModal}
        onClose={closeFormModal}
        selectedData={selectedInput}
        onUpdateSuccess={handleAddUpdateSuccess}
      />
    </React.Fragment>
  );
};
