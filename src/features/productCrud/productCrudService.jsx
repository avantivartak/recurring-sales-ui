import config from "config.json";
import http from "core/service/httpService";

export function searchProduct(body) {
  return http.get(`${config.apiEndpoint}/productTypes/search`, {
    params: body,
  });
}

export function findAll() {
  return http.get(`${config.apiEndpoint}/productTypes`);
}

// export function addCompany(body) {
//   return http.filePost(`${config.apiEndpoint}/productTypes`, body);
// }

export function addProduct(body) {
  return http.post(`${config.apiEndpoint}/productTypes`, body);
}

export function editProduct(id, body) {
  return http.put(`${config.apiEndpoint}/productTypes/${id}`, body);
}

export function deleteProduct(id) {
  return http.delete(`${config.apiEndpoint}/productTypes/${id}`);
}
