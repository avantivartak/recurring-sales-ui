import { FormCard } from "core/cards/formCard";
import { TextAreaField } from "core/components/inputWithAccessControl";
import { EditTextField } from "core/components/inputWithAccessControl";
import { Modal } from "core/components/modal";
import { useLoading } from "core/hooks/loadingHook";
import { useSubmitForm } from "core/hooks/submitFormHook";
import Joi from "joi";
import React, { useEffect } from "react";
import { toast } from "react-toastify";
import _ from "lodash";
import { addProduct, editProduct } from "./productCrudService";

export const ProductForm = ({
  open,
  onClose,
  selectedData,
  onUpdateSuccess,
}) => {
  const { loaderContainer, startLoading, stopLoading } = useLoading();

  const schema = Joi.object({
    id: Joi.number().label("Id").positive().integer().allow(0),
    name: Joi.string().trim().label("Product Name").required().min(3).max(100),
    // logo: Joi.any().label("Logo File"),
  });
  const accessToEdit = true;
  const submitCallback = async (event) => {
    // console.log(inputs);
    const { id, name } = inputs;
    try {
      startLoading();

      const body = {
        id,
        name
      };
      // console.log(companyName);
      // console.log(body);
      // let fd = new FormData();
      // fd.append("logo2", inputs["logo"], "logo2.png");
      // fd.append("name", inputs["name"]);
      // fd.append("category", inputs["category"]);
      // fd.append("contactPerson", inputs["contactPerson"]);
      // fd.append("contactNo", inputs["contactNo"]);
      // fd.append("address", inputs["address"]);

      // console.log(fd);
      // for (var pair of fd.entries()) {
      //   console.log(pair[0] + ", " + pair[1]);
      // }
      if (
        id === null ||
        id === undefined ||
        id === "" ||
        id === "0" ||
        id === 0
      ) {
        const { data: product } = await addProduct(body);
        onUpdateSuccess(product);
        toast.success("Product Added Successfully");
      } else {
        const { data: product } = await editProduct(id, body);
        onUpdateSuccess(product);
        toast.success("Product Edited Successfully");
      }
      //   console.log(companies);
      stopLoading();
    } catch (error) {
      console.log(error);
      toast.error("Error adding / editing Product details");
      stopLoading();
    }
    onClose();
  };
  let defaultInputs = { id: 0 };
  useEffect(() => {
    if (selectedData) {
      handleInputChange("id", selectedData["id"]);
      handleInputChange("name", selectedData["name"]);
    } else {
      resetInput();
      handleInputChange("id", 0);
    }
  }, [selectedData]);
  const {
    inputs,
    errors,
    handleInputChange,
    handleSubmit,
    resetInput,
    additionalValidation,
  } = useSubmitForm(schema, submitCallback, defaultInputs);

  // Category options
  
  const FormHtml = (
    <FormCard
      formName={"Add/Update Product"}
      onSubmit={handleSubmit}
      onFormReset={resetInput}
      submitAccess={accessToEdit}
    >
      <div className="columns is-multiline">
        <div className="column is-one-third">
          <EditTextField
            identifier="id"
            labelName="Id"
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
          />
        </div>

        <div className="column is-one-third">
          <EditTextField
            identifier="name"
            labelName="Product Name"
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
          />
        </div>
        {/* /.column */}
        
        {/* /.column */}
        {/* /.column */}
        {/* /.column */}
        {/* <div className="column is-one-third">
          <FileUpload
            identifier="logo"
            labelName="Logo"
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
          />
        </div> */}
        {/* /.column */}
        {/* /.column */}
      </div>
      {/* /.columns */}
    </FormCard>
  );

  return (
    <Modal open={open} onClose={onClose} width="full-width">
      <div>{FormHtml}</div>
      {loaderContainer}
    </Modal>
  );
};
