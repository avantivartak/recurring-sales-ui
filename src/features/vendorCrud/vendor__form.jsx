import { FormCard } from "core/cards/formCard";
import { EditTextField } from "core/components/inputWithAccessControl";
import { Modal } from "core/components/modal";
import { useLoading } from "core/hooks/loadingHook";
import { useSubmitForm } from "core/hooks/submitFormHook";
import Joi from "joi";
import React, { useEffect } from "react";
import { toast } from "react-toastify";
import _ from "lodash";
import { addVendor, editVendor } from "./vendorCrudService";

export const VendorForm = ({ open, onClose, selectedData, onUpdateSuccess }) => {

  const { loaderContainer, startLoading, stopLoading } = useLoading();

  const schema = Joi.object({
    id: Joi.number().label("Id").optional(),
    name: Joi.string().trim().label("Vendor Name").required(),
  });
  const accessToEdit = true;
  const submitCallback = async (event) => {
    // console.log(inputs);
    const {
      id,
      name,
    } = inputs;
    try {
      startLoading();

      const body = {
        id,
        name,
      };

      if (
        id === null ||
        id === undefined ||
        id === "" ||
        id === "0" ||
        id === 0
      ) {
        const { data: vendor } = await addVendor(body);
        onUpdateSuccess(vendor);
        toast.success("Vendor Added Successfully");
      } else {
        const { data: vendor } = await editVendor(id, body);
        onUpdateSuccess(vendor);
        toast.success("Vendor Edited Successfully");
      }
      //   console.log(companies);
      stopLoading();
    } catch (error) {
      console.log(error);
      toast.error("Error adding / editing Vendor details");
      stopLoading();
    }
    onClose();
  };
  useEffect(() => {
    if (!_.isEmpty(selectedData)) {
      console.log(selectedData);
      handleInputChange("id", selectedData["id"]);
      handleInputChange("name", selectedData["name"]);
    } else {
      resetInput();
      handleInputChange("id", 0);
    }
  }, [selectedData]);

  const {
    inputs,
    errors,
    handleInputChange,
    handleSubmit,
    resetInput,
    additionalValidation,
  } = useSubmitForm(schema, submitCallback);

  const FormHtml = (
    <FormCard
      formName={"Add/Update Vendor"}
      onSubmit={handleSubmit}
      onFormReset={resetInput}
      submitAccess={accessToEdit}
    >
      <div className="columns is-multiline">
        <div className="column is-one-third">
          <EditTextField
            identifier="id"
            labelName="Id"
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
            required={false}
          />
        </div>
        {/* /.column */}
        <div className="column is-one-third">
          <EditTextField
            identifier="name"
            labelName="Vendor Name"
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
          />
        </div>
        {/* /.column */}
        {/* /.column */}
        
        {/* /.column */}

       
        {/* /.column */}
        {/* /.column */}
        {/* /.column */}


        {/* /.column */}
        {/* /.column */}
        {/* <div className="column is-half">
          <FileUpload
            identifier="logo"
            labelName="Logo"
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
          />
        </div> */}
        {/* /.column */}

        {/* /.column */}
      </div>
      {/* /.columns */}
    </FormCard>
  );

  return (
    <Modal open={open} onClose={onClose} width="full-width">
      <div>{FormHtml}</div>
      {loaderContainer}
    </Modal>
  );
};
