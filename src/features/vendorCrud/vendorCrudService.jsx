import config from "config.json";
import http from "core/service/httpService";

export function searchVendor(body) {
  return http.get(`${config.apiEndpoint}/vendors/search`, {
    params: body,
  });
}

export function findAll() {
  return http.get(`${config.apiEndpoint}/vendors`);
}

// export function addSite(body) {
//   return http.filePost(`${config.apiEndpoint}/vendor`, body);
// }

export function addVendor(body) {
  return http.post(`${config.apiEndpoint}/vendors`, body);
}

export function editVendor(id, body) {
  return http.put(`${config.apiEndpoint}/vendors/${id}`, body);
}

export function deleteVendor(id) {
  return http.delete(`${config.apiEndpoint}/vendors/${id}`);
}
