import React, { useState } from "react";
import _ from "lodash";
import { VendorTable } from "./vendor__table";
import { VendorSearch } from "./vendor__search";
import { VendorForm } from "./vendor__form";
import { useModal } from "core/hooks/modalHook";
import { toast } from "react-toastify";
import { deleteVendor } from "./vendorCrudService";

export const VendorCrud = () => {
  const [vendor, setVendor] = useState([]);
  const [action, setAction] = useState("");
  const [selectedInput, setSelectedInput] = useState();
  const [selectedId, setSelectedId] = useState("");
  const handleSearchResults = (output) => {
    // console.log(output);
    setVendor(output);
  };

  // Add Action
  const handleAddNew = () => {
    setAction("ADD");
    setSelectedId(0);
    setSelectedInput({});
    openFormModal();
  };

  // Edit Action
  const handleEdit = (data) => {
    setAction("EDIT");
    setSelectedId(data["id"]);
    setSelectedInput(data);
    openFormModal();
  };

  // Delete Action
  const handleDelete = async (data) => {
    // console.log(data);
    try {
      setAction("DELETE");
      setSelectedId(data["id"]);
      setSelectedInput(data);
      const { data: company } = await deleteVendor(data["id"]);
      toast.success("Vendor deleted successfully");
      const newList = [...vendor];
      _.remove(newList, function (n) {
        // console.log("Checking " + data["id"] + " " + n["id"]);
        return n["id"] == data["id"];
      });
      // console.log(newList);
      setVendor(newList);
    } catch (error) {
      console.log(error);
      toast.error("Error deleting company");
    }
  };

  // Update on success
  const handleAddUpdateSuccess = (updatedData) => {
    if (action === "EDIT") {
      const index = _.findIndex(vendor, { id: selectedId });
      const updatedDataList = [...vendor];
      updatedDataList[index] = updatedData;
      setVendor(updatedDataList);
    } else if (action === "ADD") {
      const newList = [...vendor];
      newList.push(updatedData);
      setVendor(newList);
    }
  };

  const [formModal, openFormModal, closeFormModal] = useModal(false);

  return (
    <React.Fragment>
      <VendorSearch
        onSearchResults={handleSearchResults}
        onAddNew={handleAddNew}
      />
      <VendorTable data={vendor} onEdit={handleEdit} onDelete={handleDelete} />
      <VendorForm
        open={formModal}
        onClose={closeFormModal}
        selectedData={selectedInput}
        onUpdateSuccess={handleAddUpdateSuccess}
      />
    </React.Fragment>
  );
};
