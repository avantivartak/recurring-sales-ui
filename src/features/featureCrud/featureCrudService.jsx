import config from "config.json";
import http from "core/service/httpService";

export function searchFeature(body) {
  // console.log(body)
  return http.get(`${config.apiEndpoint}/features/search?featureName=${body.name}`);
}

export function findAll(body) {
  return http.get(`${config.apiEndpoint}/features`, {
    params: body,
  });
}

// export function addFeature(body) {
//   return http.filePost(`${config.apiEndpoint}/features`, body);
// }

export function addFeature(body) {
  return http.post(`${config.apiEndpoint}/features`, body);
}

export function editFeature(id, body) {
  return http.put(`${config.apiEndpoint}/features/${id}`, body);
}

export function deleteFeature(id) {
  return http.delete(`${config.apiEndpoint}/features/${id}`);
}

export function fetchAllPermissions(body) {
  return http.get(`${config.apiEndpoint}/permissions`, {
    params: body,
  });
}

export function assignPermissions(id, body) {
  return http.put(`${config.apiEndpoint}/features/${id}/permissions`, body);
}
