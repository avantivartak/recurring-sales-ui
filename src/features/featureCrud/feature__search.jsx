import React from "react";
import Joi from "joi";
import { SearchAndAddCard } from "core/cards/searchAndAddCard";
import { useLoading } from "core/hooks/loadingHook";
import { toast } from "react-toastify";
import _ from "lodash";
import { searchFeature } from "./featureCrudService";
import { useSubmitForm } from "core/hooks/submitFormHook";
import { findAll } from "./featureCrudService";
import { SelectContainer } from "core/components/inputWithAccessControl";
import { useMasterListState } from "features/masterList/masterListProvider";
import { addDefaultOption } from "core/utility/util";
import { EditTextField } from "core/components/inputWithAccessControl";

export const FeatureSearch = ({ onSearchResults, onAddNew }) => {
  const { loaderContainer, startLoading, stopLoading } = useLoading();
  const schema = Joi.object({
    name: Joi.string().trim().label("Name").allow(""),
  });
  const submitCallback = async (event) => {
    const { name } = inputs;
    try {
      startLoading();
      const body = {
        name: _.isNil(name) ? "" : name,
      };
      // console.log(featureName);
      // console.log(body);
      const { data: features } = await searchFeature(body);
      onSearchResults(features);
      //   console.log(features);
      if (features.length > 0) {
        toast.success("Data fetched");
      } else {
        toast.warn("No data found");
      }

      stopLoading();
    } catch (error) {
      console.log(error);
      if (error.response && error.response.status === 400) {
        toast.warn("No data found");
      } else {
        toast.error("Error fetching feature data");
      }
      stopLoading();
    }
  };
  const defaultInputs = {};
  const {
    inputs,
    errors,
    handleInputChange,
    handleSubmit,
    resetInput,
    additionalValidation,
  } = useSubmitForm(schema, submitCallback, defaultInputs);

  const handleFindAll = async () => {
    try {
      startLoading();
      // console.log(featureName);
      // console.log(body);
      const { data: features } = await findAll();
      onSearchResults(features);
      // console.log(features);
      if (features.length > 0) {
        toast.success("Data fetched");
      } else {
        toast.warn("No data found");
      }

      stopLoading();
    } catch (error) {
      console.log(error);
      if (error.response && error.response.status === 400) {
        toast.warn("No data found");
      } else {
        toast.error(
          "Error fetching feature data. Please refer console for detailed error"
        );
      }
      stopLoading();
    }
  };

  const handleAddNew = () => {
    onAddNew();
  };

  return (
    <React.Fragment>
      <SearchAndAddCard
        formName={"Search Feature"}
        onSubmit={handleSubmit}
        onFormReset={resetInput}
        onAddNew={handleAddNew}
        onFindAll={handleFindAll}
        submitAccess={true}
        submitText={"Search"}
      >
        <div className="columns is-multiline">
          <div className="column is-one-third">
            <EditTextField
              identifier="name"
              labelName="Name"
              handleInputChange={handleInputChange}
              editAccess={true}
              inputs={inputs}
              errors={errors}
            />
          </div>
          {/* /.column */}
        </div>
        {/* /.columns */}
      </SearchAndAddCard>
      {loaderContainer}
    </React.Fragment>
  );
};
