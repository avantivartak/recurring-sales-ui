import config from "config.json";
import http from "core/service/httpService";

export function searchPermission(body) {
  console.log(body)
  return http.get(`${config.apiEndpoint}/permissions/search?permissionName=${body.name}`);
}

export function findAll(body) {
  return http.get(`${config.apiEndpoint}/permissions`, {
    params: body,
  });
}

// export function addPermission(body) {
//   return http.filePost(`${config.apiEndpoint}/permissions`, body);
// }

export function addPermission(body) {
  return http.post(`${config.apiEndpoint}/permissions`, body);
}

export function editPermission(id, body) {
  return http.put(`${config.apiEndpoint}/permissions/${id}`, body);
}

export function deletePermission(id) {
  return http.delete(`${config.apiEndpoint}/permissions/${id}`);
}
