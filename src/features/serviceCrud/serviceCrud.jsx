import React, { useState } from "react";
import _ from "lodash";
import { ServiceTable } from "./service__table";
import { ServiceSearch } from "./service__search";
import { ServiceForm } from "./service__form";
import { useModal } from "core/hooks/modalHook";
import { toast } from "react-toastify";
import { deleteService } from "./serviceCrudService";
import { AddBillingForm } from "./add__billing__form";
import { deleteBilling } from "features/billingCrud/billingCrudService";
import { BillingHistoryTable } from "./billing_history";

export const ServiceCrud = () => {
  const [nodes, setNodes] = useState([]);
  const [action, setAction] = useState("");
  const [selectedInput, setSelectedInput] = useState();
  const [selectedId, setSelectedId] = useState(0);
  const [billing, setBilling] = useState([]);
  const [billingAction, setBillingAction] = useState("");
  const [selectedBillingInput, setSelectedBillingInput] = useState();
  const [selectedBillingId, setSelectedBillingId] = useState(0);

  // Modal handlers
  const [settingsUploadForm, openBillingModal, closeSettingsUploadModal] = useModal(false);
  const [formModal, openFormModal, closeFormModal] = useModal(false);
  const [billingHistoryModal, openBillingHistoryModal, closeBillingHistoryModal] = useModal(false);

  const handleSearchResults = (output) => {
    setNodes(output);
  };

  const handleAddNew = () => {
    setAction("ADD");
    setSelectedId(0);
    setSelectedInput({});
    openFormModal();
  };

  const handleEdit = (data) => {
    setAction("EDIT");
    setSelectedId(data["id"]);
    setSelectedInput(data);
    openFormModal();
  };

  const handleDelete = async (data) => {
    try {
      setAction("DELETE");
      setSelectedId(data["id"]);
      setSelectedInput(data);
      await deleteService(data["id"]);
      toast.success("Service deleted successfully");
      const newList = [...nodes];
      _.remove(newList, (n) => n["id"] === data["id"]);
      setNodes(newList);
    } catch (error) {
      console.log(error);
      toast.error("Error deleting node");
    }
  };

  const handleAddUpdateSuccess = (updatedData) => {
    if (action === "EDIT") {
      const index = _.findIndex(nodes, { id: selectedId });
      const updatedDataList = [...nodes];
      updatedDataList[index] = updatedData;
      setNodes(updatedDataList);
    } else if (action === "ADD") {
      const newList = [...nodes];
      newList.push(updatedData);
      setNodes(newList);
    }
  };

  const handleBilling = (data) => {
    setBillingAction("ADD");
    setSelectedBillingId(0);
    setSelectedBillingInput({
      companyId: data.companyId,
      serviceId: data.id  
    });
    openBillingModal();
  };
  
  const handleBillingEdit = (data) => {
    setBillingAction("EDIT");
    setSelectedBillingId(data["id"]);
    setSelectedBillingInput({
      ...data,
      companyId: data["companyId"],
      serviceId: data["id"],
      lastInvoiceAmount: data ["lastInvoiceAmount"],
      lastInvoiceNo: data ["lastInvoiceNo"],
      lastInvoiceDate: data ["lastInvoiceDate"]
    });
    openBillingModal();
  };
  
  const handleBillingDelete = async (data) => {
    try {
      setBillingAction("DELETE");
      setSelectedBillingId(data["id"]);
      setSelectedBillingInput(data);
      await deleteBilling(data["id"]);
      toast.success("Billing deleted successfully");
      const newList = [...billing];
      _.remove(newList, (n) => n["id"] === data["id"]);
      setBilling(newList);
    } catch (error) {
      console.log(error);
      toast.error("Error deleting Billing");
    }
  };

  const handleAddUpdateBillingSuccess = (updatedData) => {
    if (billingAction === "EDIT") {
      const index = _.findIndex(billing, { id: selectedBillingId });
      const updatedDataList = [...billing];
      updatedDataList[index] = updatedData;
      setBilling(updatedDataList);
    } else if (billingAction === "ADD") {
      const newList = [...billing];
      newList.push(updatedData);
      setBilling(newList);
    }
  };

  const handleBillingHistory = (data) => {
    // Fetch billing history based on service ID (data.id)
    // For this example, assuming `fetchBillingHistory` is a function that retrieves billing history data
    const fetchBillingHistory = async (serviceId) => {
      try {
        // Replace with your actual fetching logic
        // Example: const response = await fetch(`/api/billing/history/${serviceId}`);
        // const result = await response.json();
        const result = []; // Replace with actual result
        setBilling(result);
        openBillingHistoryModal();
      } catch (error) {
        console.log(error);
        toast.error("Error fetching billing history");
      }
    };

    fetchBillingHistory(data.id);
  };

  return (
    <React.Fragment>
      <ServiceSearch
        onSearchResults={handleSearchResults}
        onAddNew={handleAddNew}
      />
      <ServiceTable
        data={nodes}
        onEdit={handleEdit}
        onDelete={handleDelete}
        onBilling={handleBilling}
        onBillingHistory={handleBillingHistory} 
      />
      <ServiceForm
        open={formModal}
        onClose={closeFormModal}
        selectedData={selectedInput}
        onUpdateSuccess={handleAddUpdateSuccess}
      />
      <AddBillingForm
        open={settingsUploadForm}
        onClose={closeSettingsUploadModal}
        selectedData={selectedBillingInput}
        onUpdateSuccess={handleAddUpdateBillingSuccess}
      />
      <BillingHistoryTable
        open={billingHistoryModal}
        onClose={closeBillingHistoryModal}
        data={billing}
        onEdit={handleBillingEdit}
        onDelete={handleBillingDelete}
      />
    </React.Fragment>
  );
};
