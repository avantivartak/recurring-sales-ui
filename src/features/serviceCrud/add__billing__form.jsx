import { FormCard } from "../../core/cards/formCard";
import { useLoading } from "../../core/hooks/loadingHook";
import { useSubmitForm } from "../../core/hooks/submitFormHook";
import Joi from "joi";
import React, { useEffect } from "react";
import { toast } from "react-toastify";
import _ from "lodash";
import { DateField, EditTextField, TextAreaField } from "core/components/inputWithAccessControl";
import { addProduct, editProduct } from "features/productCrud/productCrudService";
import { useMasterListState } from "features/masterList/masterListProvider";
import { editBilling } from "features/billingCrud/billingCrudService";
import { Modal } from "core/components/modal";
import { addBilling } from "./serviceCrudService";

export const AddBillingForm = ({ open, onClose, selectedData, onUpdateSuccess }) => {
  // console.log("bill selected data:",selectedData)
    const { loaderContainer, startLoading, stopLoading } = useLoading();
    
    const schema = Joi.object({
        id: Joi.number().label("Id").positive().allow(0),
        serviceId: Joi.number().label("Service Id").positive().allow(0),
        companyId: Joi.number().label("Company Id").positive().allow(0),
        remarks: Joi.string().trim().label("Remarks").allow(''),
        expectedAmount: Joi.number().positive().label("Expected Amount").allow(0),
        lastInvoiceAmount: Joi.number().positive().label("Last Invoice Amount").allow(0),
        lastInvoiceNo: Joi.string().trim().label("Last Invoice Number").allow(''),
        lastInvoiceDate: Joi.date().label("Last Invoice Date").allow(''),
        invoiceAmount: Joi.number().positive().custom((value, helpers) => {
            const regex = /^\d+(\.\d{1,2})?$/;
            if (!regex.test(value.toString())) {
                return helpers.message('"Invoice Amount" must be a number with up to two decimal places');
            }
            return value;
        }).label("Invoice Amount").required(),
        invoiceNo: Joi.string().trim().label("Invoice Number").required(),
        invoiceDate: Joi.date().label("Invoice Date").required(),
    });
    
    const accessToEdit = true;
    const { companyList } = useMasterListState();
    
    const submitCallback = async () => {
        const { id, expectedAmount, lastInvoiceAmount, lastInvoiceNo, lastInvoiceDate, invoiceAmount, invoiceNo, invoiceDate, remarks, serviceId,companyId } = inputs;
        const companyObj = _.find(companyList, { id: _.toInteger(inputs.companyId) });
        try {
            startLoading();
            
            const body = {
                id,
                expectedAmount,
                invoiceAmount,
                invoiceNo,
                invoiceDate,
                remarks,
                // companyName: `${companyObj?.name} - ${companyObj?.plotNo}`,
                company: { id: companyId },
                service: { id: serviceId }
            };
            console.log("body:",body)

            if (id === null || id === undefined || id === "" || id === "0" || id === 0) {
                const { data: billing } = await addBilling(body);
                onUpdateSuccess(billing);
                toast.success("Billing Added Successfully");
            } else {
                const { data: billing } = await editBilling(id, body);
                onUpdateSuccess(billing);
                toast.success("Billing Edited Successfully");
            }
            stopLoading();
        } catch (error) {
            console.log(error);
            toast.error("Error adding / editing Billing details");
            stopLoading();
        }
        onClose();
    };

    const defaultInputs = {
        // id: 0,
        // serviceId: 0,
        // expectedAmount: 1000,
        // companyId: 0,
    };
    
    useEffect(() => {
        if (selectedData) {
            // Populate form fields with selectedData if available
          //   handleInputChange("id", selectedData["id"]);
          // handleInputChange("serviceId", selectedData["serviceId"]);
          // handleInputChange("expectedAmount", selectedData["expectedAmount"]);
          handleInputChange("lastInvoiceAmount", selectedData["lastInvoiceAmount"]);
          handleInputChange("lastInvoiceNo", selectedData["lastInvoiceNo"]);
          handleInputChange("lastInvoiceDate", selectedData["lastInvoiceDate"]);
          handleInputChange("invoiceAmount", selectedData["invoiceAmount"]);
          handleInputChange("invoiceNo", selectedData["invoiceNo"]);
          handleInputChange("invoiceDate", selectedData["invoiceDate"]);
          handleInputChange("remarks", selectedData["remarks"]);

            Object.keys(selectedData).forEach(key => {
                handleInputChange(key, selectedData[key]);
            });
        } else {
            // Reset inputs to default values
            resetInput();
            handleInputChange("id", 0);
            handleInputChange("serviceId", 0);
            handleInputChange("expectedAmount", 1000);
            // Object.keys(defaultInputs).forEach(key => {
            //     handleInputChange(key, defaultInputs[key]);
            // });
        }
    }, [selectedData]);

    const {
        inputs,
        errors,
        handleInputChange,
        handleSubmit,
        resetInput,
    } = useSubmitForm(schema, submitCallback, defaultInputs);
    
    const today = new Date();
    const maxDate = new Date(today);
    maxDate.setFullYear(maxDate.getFullYear() + 20);
    
    return (
        <Modal open={open} onClose={onClose} width="full-width">
            <FormCard
                formName={"Add New Bill"}
                onSubmit={handleSubmit}
                onFormReset={resetInput}
                submitAccess={accessToEdit}
            >
                <div className="columns is-multiline">
                    <div className="column is-one-quarter">
                        <EditTextField
                            identifier="id"
                            labelName="Id"
                            handleInputChange={handleInputChange}
                            inputs={inputs}
                            errors={errors}
                        />
                    </div>
                    {/* <div className="column is-one-quarter">
                        <EditTextField
                            identifier="serviceId"
                            labelName="Service Id"
                            handleInputChange={handleInputChange}
                            inputs={inputs}
                            errors={errors}
                        />
                    </div> */}
                    <div className="column is-one-quarter">
                        <EditTextField
                            identifier="expectedAmount"
                            labelName="Expected Amount"
                            handleInputChange={handleInputChange}
                            inputs={inputs}
                            errors={errors}
                        />
                    </div>
                    <div className="column is-one-quarter">
                        <EditTextField
                            identifier="lastInvoiceAmount"
                            labelName="Last Invoice Amount"
                            handleInputChange={handleInputChange}
                            inputs={inputs}
                            errors={errors}
                        />
                    </div>
                    <div className="column is-one-quarter">
                        <EditTextField
                            identifier="lastInvoiceNo"
                            labelName="Last Invoice Number"
                            handleInputChange={handleInputChange}
                            inputs={inputs}
                            errors={errors}
                        />
                    </div>
                    <div className="column is-one-quarter">
                        <EditTextField
                            identifier="lastInvoiceDate"
                            labelName="Last Invoice Date"
                            handleInputChange={handleInputChange}
                            inputs={inputs}
                            errors={errors}
                        />
                    </div>
                    <div className="column is-one-quarter">
                        <EditTextField
                            identifier="invoiceAmount"
                            labelName="Invoice Amount"
                            editAccess={true}
                            handleInputChange={handleInputChange}
                            inputs={inputs}
                            errors={errors}
                        />
                    </div>
                    <div className="column is-one-quarter">
                        <EditTextField
                            identifier="invoiceNo"
                            labelName="Invoice Number"
                            editAccess={true}
                            handleInputChange={handleInputChange}
                            inputs={inputs}
                            errors={errors}
                        />
                    </div>
                    <div className="column is-one-quarter">
                        <DateField
                            identifier="invoiceDate"
                            labelName="Invoice Date (yyyy-mm-dd)"
                            handleInputChange={handleInputChange}
                            max={maxDate}
                            inputs={inputs}
                            errors={errors}
                            editAccess={true}
                        />
                    </div>
                    <div className="column is-full">
                        <TextAreaField
                            identifier="remarks"
                            labelName="Remarks"
                            handleInputChange={handleInputChange}
                            required={false}
                            inputs={inputs}
                            errors={errors}
                            editAccess={true}
                            rows={2}
                        />
                    </div>
                </div>
                {loaderContainer}
            </FormCard>
        </Modal>
    );
};
