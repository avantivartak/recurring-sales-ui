import React, { useEffect, useState } from "react";
import { useSubmitForm } from "core/hooks/submitFormHook";
import Joi from "joi";
import { FormCard } from "core/cards/formCard";
import { EditTextField } from "core/components/inputWithAccessControl";
import _ from "lodash";
import { searchNodeByNodeType } from "./serviceCrudService";
import { addDefaultOption } from "core/utility/util";
import { formatArrayToOptions } from "core/utility/util";
import { SelectContainer } from "core/components/inputWithAccessControl";
import { useMasterListState } from "features/masterList/masterListProvider";
import { toast } from "react-toastify";
import { useLoading } from "core/hooks/loadingHook";
import { saveNodeParameters } from "./serviceCrudService";

export const ParameterArrayForm = ({ selectedData, onClose }) => {
  const { parameters, nodeType, id } = selectedData;

  // console.log(selectedData);
  const accessToEdit = true;

  const saveParameterData = async (result) => {
    try {
      startLoading();
      const body = { id: id, parameters: result };
      // console.log(body);
      const { data: parameter } = await saveNodeParameters(id, body);
      toast.success("Parameter Added Successfully");

      stopLoading();
    } catch (error) {
      console.log(error);
      toast.error("Error adding / editing parameter details");
      stopLoading();
    }
    onClose();
  };

  const handleSubmit = async (event) => {
    if (event) {
      event.preventDefault();
    }
    // console.log(parameterList);
    const result = schema.validate(parameterList, { abortEarly: false });
    if (result.error) {
      // console.log(result);
      toast.error("Validation Error");
    } else {
      // console.log(result);
      saveParameterData(result.value);
    }
  };

  const objectSchema = Joi.object({
    id: Joi.number().label("Id").positive().integer().allow(0),
    name: Joi.string().trim().label("Parameter Name").required(),
    identifier: Joi.string().trim().label("Identifier").required(),
    deviceType: Joi.string().trim().label("Device Type").required(),
    intervalType: Joi.string().trim().label("Interval Type").required(),
    limitType: Joi.string().trim().label("Limit Type").required(),
    limitMin: Joi.number().label("Limit Min").required(),
    limitMax: Joi.number().label("Limit Max").required(),
    rangeMin: Joi.number().label("Range Min").required(),
    rangeMax: Joi.number().label("Range Max").required(),
    unit: Joi.string().trim().label("Unit").allow("").required(),
  });
  const schema = Joi.array().items(objectSchema).min(1).unique().required();
  const [parameterList, setParameterList] = useState([]);
  useEffect(() => {
    if (parameters) {
      setParameterList(parameters);
    }
  }, [parameters]);
  const handleAddRow = () => {
    // console.log("Add row clicked");
    const newList = [...parameterList];
    newList.push({});
    setParameterList(newList);
  };
  const handleEditRow = (arrayIndex, name, value) => {
    // console.log(`Row edited ${arrayIndex} ${name} ${value}`);
    const updatedData = parameterList[arrayIndex];
    updatedData[name] = value;
    const updatedDataList = [...parameterList];
    updatedDataList[arrayIndex] = updatedData;
    setParameterList(updatedDataList);
  };
  const handleDeleteRow = (arrayIndex) => {
    // console.log(`Delete row clicked ${arrayIndex}`);
    const newList = [...parameterList];
    newList.splice(arrayIndex, 1);
    // console.log(newList);
    setParameterList(newList);
  };

  const options = {
    buttons: (
      <button
        type="button"
        className="button is-info is-small mx-2"
        onClick={handleAddRow}
      >
        Add Parameter Row
      </button>
    ),
  };
  let parameterRowList = [];
  _.forEach(parameterList, (p, index) => {
    // console.log(p);
    parameterRowList.push(
      <ParameterRow
        key={index}
        arrayIndex={index}
        schema={objectSchema}
        nodeType={nodeType}
        parameter={p}
        onRowEdit={handleEditRow}
        onRowDelete={handleDeleteRow}
      />
    );
  });

  const { loaderContainer, startLoading, stopLoading } = useLoading();
  return (
    <React.Fragment>
      <FormCard
        formName={"Add/Update Parameter"}
        onSubmit={handleSubmit}
        submitAccess={accessToEdit}
        options={options}
      >
        {parameterRowList}
      </FormCard>
      {loaderContainer}
    </React.Fragment>
  );
};

const ParameterRow = ({
  arrayIndex,
  schema,
  nodeType,
  parameter,
  onRowEdit,
  onRowDelete,
}) => {
  const [parameterList, setParameterList] = useState([]);
  const { parameterList: masterParameterList } = useMasterListState();

  // Interval Type Options
  const limitTypes = ["NA", "UPPER", "LOWER", "RANGE"];
  const limitTypeOptions = addDefaultOption(formatArrayToOptions(limitTypes));

  useEffect(() => {
    if (nodeType) {
      const data = _.filter(masterParameterList, { deviceType: nodeType });
      // console.log(data);
      setParameterList(data);
    }
  }, [nodeType]);

  const parameterNames = _.map(parameterList, "name");
  const parameterOptions = addDefaultOption(
    formatArrayToOptions(parameterNames)
  );
  const submitCallback = () => {};
  let defaultInputs = {};
  useEffect(() => {
    if (parameter) {
      const {
        id,
        name,
        identifier,
        deviceType,
        intervalType,
        limitType,
        limitMin,
        limitMax,
        rangeMin,
        rangeMax,
        unit,
      } = parameter;
      handleInputChange("id", id);
      handleInputChange("name", name);
      handleInputChange("identifier", identifier);
      handleInputChange("deviceType", deviceType);
      handleInputChange("intervalType", intervalType);
      handleInputChange("limitType", limitType);
      handleInputChange("limitMin", limitMin);
      handleInputChange("limitMax", limitMax);
      handleInputChange("rangeMin", rangeMin);
      handleInputChange("rangeMax", rangeMax);
      handleInputChange("unit", unit);
    }
  }, [parameter]);

  const { inputs, errors, handleInputChange } = useSubmitForm(
    schema,
    submitCallback,
    defaultInputs
  );

  // Custom input change
  const handleCustomInputChange = (name, value) => {
    onRowEdit(arrayIndex, name, value);
    handleInputChange(name, value);
  };

  // Parameter option change
  const handleParameterNameChange = (name, value) => {
    const selectedParameter = _.find(parameterList, { name: value });
    // console.log(selectedParameter);
    const {
      id,
      identifier,
      deviceType,
      intervalType,
      limitType,
      lowerLimit: limitMin,
      upperLimit: limitMax,
      lowerLimit: rangeMin,
      upperLimit: rangeMax,
      unit,
    } = selectedParameter;
    handleCustomInputChange("identifier", identifier);
    handleCustomInputChange("deviceType", deviceType);
    handleCustomInputChange("intervalType", intervalType);
    handleCustomInputChange("limitType", limitType);
    handleCustomInputChange("limitMin", limitMin);
    handleCustomInputChange("limitMax", limitMax);
    handleCustomInputChange("rangeMin", rangeMin);
    handleCustomInputChange("rangeMax", rangeMax);
    handleCustomInputChange("unit", unit);
    handleCustomInputChange(name, value);
  };
  return (
    <div className="box">
      <div className="columns is-multiline">
        <div className="column is-one-third">
          <EditTextField
            identifier="id"
            labelName="Id"
            handleInputChange={handleCustomInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
          />
        </div>
        {/* /.column */}
        <div className="column is-one-third">
          <SelectContainer
            identifier="name"
            labelName="Name"
            handleInputChange={handleParameterNameChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
            options={parameterOptions}
          />
        </div>
        {/* /.column */}
        <div className="column is-one-third">
          <SelectContainer
            identifier="limitType"
            labelName="Limit Type"
            handleInputChange={handleCustomInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
            options={limitTypeOptions}
          />
        </div>
        {/* /.column */}
        <div className="column is-one-third">
          <EditTextField
            identifier="limitMin"
            labelName="Limit Min"
            handleInputChange={handleCustomInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
          />
        </div>
        {/* /.column */}
        <div className="column is-one-third">
          <EditTextField
            identifier="limitMax"
            labelName="Limit Max"
            handleInputChange={handleCustomInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
          />
        </div>
        {/* /.column */}
        <div className="column is-one-third">
          <EditTextField
            identifier="rangeMin"
            labelName="Range Min"
            handleInputChange={handleCustomInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
          />
        </div>
        {/* /.column */}
        <div className="column is-one-third">
          <EditTextField
            identifier="rangeMax"
            labelName="Range Max"
            handleInputChange={handleCustomInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
          />
        </div>
        {/* /.column */}
        <div className="column is-one-third">
          <EditTextField
            identifier="unit"
            labelName="Unit"
            handleInputChange={handleCustomInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
          />
        </div>
        {/* /.column */}

        <div className="column is-full">
          <button
            type="button"
            className="m-2 button is-small is-danger"
            onClick={() => onRowDelete(arrayIndex)}
          >
            Delete Row
          </button>
        </div>
        {/* /.column */}
      </div>
      {/* /.columns */}
    </div>
  );
};
