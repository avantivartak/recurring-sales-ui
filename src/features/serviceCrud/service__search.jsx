import React from "react";
import Joi from "joi";
import { SearchAndAddCard } from "core/cards/searchAndAddCard";
import { useLoading } from "core/hooks/loadingHook";
import { toast } from "react-toastify";
import _ from "lodash";
import { searchService } from "./serviceCrudService";
import { useSubmitForm } from "core/hooks/submitFormHook";
import { findAll } from "./serviceCrudService";
import { useMasterListState } from "features/masterList/masterListProvider";
import { addDefaultOption } from "core/utility/util";
import { SearchableSelectContainer } from "core/components/inputWithAccessControl";

export const ServiceSearch = ({ onSearchResults, onAddNew }) => {
  const { companyList } = useMasterListState();
  // console.log(companyList)
  // Site Options
  const sortedSiteList = _.sortBy(companyList, ["name","plotNo"]);
const siteOptions = _.map(sortedSiteList, site => {
  const { id, name,plotNo } = site;
  return { label: `${name} - ${plotNo}`, value: _.toString(id) };
});
const siteNameOptions = addDefaultOption(siteOptions);


  const { loaderContainer, startLoading, stopLoading } = useLoading();
  const schema = Joi.object({
    name: Joi.string().trim().label("Product Type").allow(""),
  });
  const submitCallback = async (event) => {
    const { name } = inputs;
    try {
      startLoading();
      const body = {
        name: _.isNil(name) ? "" : name,
      };
  
      const { data: products } = await searchService(body);
      onSearchResults(products);
        // console.log("products:",products);
      if (products.length > 0) {
        toast.success("Data fetched");
      } else {
        toast.warn("No data found");
      }

      stopLoading();
    } catch (error) {
      console.log(error);
      if (error.response && error.response.status === 400) {
        toast.warn("No data found");
      } else {
        toast.error("Error fetching company data");
      }
      stopLoading();
    }
  };
  const defaultInputs = {};
  const {
    inputs,
    errors,
    handleInputChange,
    handleSubmit,
    resetInput,
    additionalValidation,
  } = useSubmitForm(schema, submitCallback, defaultInputs);

  const handleFindAll = async () => {
    try {
      startLoading();
      // console.log(nodeName);
      // console.log(body);
      const { data: products } = await findAll();
      onSearchResults(products);
      if (products.length > 0) {
        toast.success("Data fetched");
      } else {
        toast.warn("No data found");
      }

      stopLoading();
    } catch (error) {
      console.log(error);
      if (error.response && error.response.status === 400) {
        toast.warn("No data found");
      } else {
        toast.error(
          "Error fetching product data. Please refer console for detailed error"
        );
      }
      stopLoading();
    }
  };

  const handleAddNew = () => {
    onAddNew();
  };

  return (
    <React.Fragment>
      <SearchAndAddCard
        formName={"Search Service"}
        onSubmit={handleSubmit}
        onFormReset={resetInput}
        onAddNew={handleAddNew}
        onFindAll={handleFindAll}
        submitAccess={true}
        submitText={"Search"}
      >
        <div className="columns is-multiline">
          <div className="column is-one-third">
            <SearchableSelectContainer
              identifier="name"
              labelName="Company Name"
              handleInputChange={handleInputChange}
              editAccess={true}
              inputs={inputs}
              errors={errors}
              options={siteNameOptions}
            />
          </div>
          {/* /.column */}
        </div>
        {/* /.columns */}
      </SearchAndAddCard>
      {loaderContainer}
    </React.Fragment>
  );
};
