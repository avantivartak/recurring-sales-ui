import config from "config.json";
import http from "core/service/httpService";

export function searchService(body) {
  return http.get(
    `${config.apiEndpoint}/services/search/company/${body.name}`
  );
}

export function findAll(body) {
  return http.get(`${config.apiEndpoint}/services`, {
    params: body,
  });
}
 
// export function addNode(body) {
//   return http.filePost(`${config.apiEndpoint}/services/platformLicense`, body);
// }

export function addService(body) {
  if ((body["productType"] == "platform license")) {
    return http.post(`${config.apiEndpoint}/services/platformLicense`, body);
  } else if ((body["productType"] == "amc")) {
    return http.post(`${config.apiEndpoint}/services/amc`, body);
  } else{
    throw "Product Type not implemented";
  }
}

export function editService(id, body) {
  if ((body["productType"] == "platform license")) {
    return http.put(`${config.apiEndpoint}/services/platformLicense/${id}`, body);
  } else if ((body["productType"] == "amc")) {
    return http.put(`${config.apiEndpoint}/services/id/${id}`, body);
  } else{
    throw "Product Type not implemented";
  }
}

export function deleteService(id) {
  return http.delete(`${config.apiEndpoint}/services/${id}`);
}
export function deleteBilling(id) {
  return http.delete(`${config.apiEndpoint}/billings/${id}`);
}
export function addBilling(body) {
  return http.post(`${config.apiEndpoint}/billings`, body);
}

export function saveNodeParameters(id, body) {
  return http.put(`${config.apiEndpoint}/nodes/${id}/parameters`, body);
}

export function uploadConfigFiles(body, deviceId) {
  return http.post(`${config.apiEndpoint}/config/upload/${deviceId}`, body);
}

export function downloadConfigFilesAsZip(deviceId, fileName) {
  return http.blobZipGet(
    `${config.apiEndpoint}/config/downloadZip/${deviceId}/${fileName}`
  );
}

export function fetchDashboardValues() {
  return http.get(`${config.apiEndpoint}/dashboard/admin`);
}

export function generateReport(body) {
  return http.blobPost(`${config.apiEndpoint}/reports/advanced`, body);
}

export function fetchFileNamesByDeviceId(deviceId) {
  return http.get(`${config.apiEndpoint}/config/${deviceId}`);
}

