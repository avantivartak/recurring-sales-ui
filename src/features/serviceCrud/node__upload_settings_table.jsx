import { TableWithGrid } from "core/components/tableWithGrid";
import React, { useEffect, useState } from "react";
import { useLoading } from "../../core/hooks/loadingHook";
import { fetchFileNamesByDeviceId } from "./serviceCrudService";

export const SettingsFileTable = ({ deviceId }) => {
    const { loaderContainer, startLoading, stopLoading } = useLoading("absolute");
    const [data, setData] = useState([]);
    const [rows, setRows] = useState();

    const columnDefs = [
        {
            headerName: "Sr. No.",
            valueGetter: (params) => params.node.rowIndex + 1, // Serial number is row index + 1
            maxWidth: 80,
        }, {
            headerName: "File Name",
            field: "fileName",
            minWidth: 150,
        },

    ];

    useEffect(() => {
        const fetchData = async () => {
            startLoading();
            try {
                const { data: files } = await fetchFileNamesByDeviceId(deviceId);
                const data = files;
                console.log(files);
                setData(data);
                setRows(data.length);
            } catch (error) {
                console.error("Error fetching files data:", error);
            }
            stopLoading();
        };

        fetchData();
    }, [deviceId, rows]);

    return (
        <React.Fragment>
            {rows > 0 &&
                <div className="card mx-2 my-2 ">
                    <TableWithGrid
                        rowData={data}
                        columnDefs={columnDefs}
                        pagination={true}
                    />
                </div>}

            {loaderContainer}

        </React.Fragment>
    );
};
