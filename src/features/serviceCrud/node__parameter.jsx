import { Modal } from "core/components/modal";
import { TabContainer } from "core/components/tab";
import { Panel } from "core/components/tab";
import React from "react";
import { ParameterArrayForm } from "./node__parameter__form";

export const NodeParameter = ({ open, onClose, selectedData }) => {
  // console.log(selectedData);
  if (!selectedData) {
    return "";
  }

  const CalParameterHtml = (
    <div className="m-2 p-2"> Calibration Parameters</div>
  );
  const FormHtml = <div className="m-2 p-2">Diagnostic Parameters</div>;
  return (
    <Modal open={open} onClose={onClose} width="full-width">
      <div className="card">
        <TabContainer selected={0}>
          <Panel title="Parameters">
            {
              <ParameterArrayForm
                selectedData={selectedData}
                onClose={onClose}
              />
            }
          </Panel>
          <Panel title="Diagnostic Parameters">{FormHtml}</Panel>
          <Panel title="Calibration Parameters">{CalParameterHtml}</Panel>
        </TabContainer>
      </div>
    </Modal>
  );
};
