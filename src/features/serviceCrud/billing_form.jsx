import { useLoading } from "../../core/hooks/loadingHook";
import React, { useEffect, useState } from "react";
import _ from "lodash";
import { Modal } from "../../core/components/modal";
import { Panel, TabContainer } from "core/components/tab";
import { AddBillingForm } from "./add__billing__form";
import { BillingHistoryTable } from "./billing_history";
import { toast } from "react-toastify";
import { deleteBilling } from "./serviceCrudService";
import { useModal } from "core/hooks/modalHook";


export const BillingForm = ({ open, onClose }) => {
 
   //Billing Action
  const [billing, setBilling] = useState([]);
  const [billingAction, setBillingAction] = useState("");
  const [selectedBillingInput, setSelectedBillingInput] = useState();
  const [selectedBillingId, setSelectedBillingId] = useState(0);
  // Edit Action
  const handleBillingEdit = (data) => {
    setBillingAction("EDIT");
    console.log("data[id]", data["id"])
    setSelectedBillingId(data["id"]);
    console.log("DATA = ", data)
    setSelectedBillingInput(data);
    openFormModal();
  };

  // Delete Action
  const handleBillingDelete = async (data) => {
    try {
      setBillingAction("DELETE");
      setSelectedBillingId(data["id"]);
      setSelectedBillingInput(data);
      const { data: node } = await deleteBilling(data["id"]);
      toast.success("Billing deleted successfully");
      const newList = [...billing];
      _.remove(newList, function (n) {
        // console.log("Checking " + data["id"] + " " + n["id"]);
        return n["id"] == data["id"];
      });
      // console.log(newList);
      setBilling(newList);
    } catch (error) {
      console.log(error);
      toast.error("Error deleting Billing");
    }
  };
  const handleAddUpdateBillingSuccess = (updatedData) => {
    if (billingAction === "EDIT") {
      const index = _.findIndex(billing, { id: selectedBillingId });
      const updatedDataList = [...billing];
      updatedDataList[index] = updatedData;
      setBilling(updatedDataList);
    } else if (billingAction === "ADD") {
      const newList = [...billing];
      newList.push(updatedData);
      setBilling(newList);
    }
  };
  const [settingsUploadForm, closeSettingsUploadModal] = useModal(false);

  const [ openFormModal] = useModal(false);
    return (
        <Modal open={open} onClose={onClose} width="full-width">
          <div className="box m-2">
            <TabContainer selected={0}>
              <Panel title="Add New Bill" >
                <AddBillingForm
                open={settingsUploadForm}
                onClose={closeSettingsUploadModal}
                selectedData={selectedBillingInput}
                onUpdateSuccess={handleAddUpdateBillingSuccess}

                />
              </Panel>
              <Panel title="Billing History">
                <BillingHistoryTable
                 data={billing}
                 onEdit={handleBillingEdit}
                 onDelete={handleBillingDelete}
                />
              </Panel>
            </TabContainer>
</div>
        </Modal>

    );
};
