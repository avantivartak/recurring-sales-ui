import { BillingHistoryIcon, BillingIcon, DeleteIcon, EditIcon } from "core/components/svgIcon";
import { Table } from "core/components/table";
import { useConfirmBox } from "core/hooks/confirmHook";
import _ from "lodash";
import React from "react";

export const ServiceTable = ({ data, onEdit, onDelete,onBilling,onBillingHistory }) => {
  // Edit action

  const handleEditAction = (value) => {
    const companyData = _.pick(value, [
     "id",
      "companyName",
      "productType"
      ]);
    onEdit(companyData);

  };
  // Delete Action

  const options = { onSuccess: onDelete };
  const { confirmContainer, openConfirmBox, setSelectedInput } =
    useConfirmBox(options);

  const handleDeleteAction = (value) => {
    setSelectedInput(value);
    openConfirmBox();
  };
  // Billing Action
  const handleBillingAction = (value) => {
    onBilling(value)
  }
  // Billing History Action
  const handleBillingHistoryAction = (value) => {
    onBillingHistory(value)
  }
  const defaultColDef = {
    sortable: true,
    resizable: true,
    filter: true,
    floatingFilter: true,
    flex: 1,
    minWidth: 150,
  };

  const columnDefs = [
    {
      headerName: "Service",
      children: [
        {
          headerName: "Id",
          field: "id",
        },
        {
          headerName: "Company Name",
          field: "companyName",
          maxWidth: 550,
        },
        {
          headerName: "Product Type",
          field: "productType",
          maxWidth: 550,
        },
        {
          headerName: "Details",
          valueGetter: function (params) {
            return params.data;
          },
          cellRenderer: "actionBtnCellRenderer",
          sortable: false,
          filter: false,
          minWidth: 300,
        },
      ],
    },
  ];

  const ActionButtonRenderer = ({ value }) => {
    return (
      <React.Fragment>
        <span
          className="mx-2 is-clickable"
          onClick={() => handleEditAction(value)}
        >
          <EditIcon />
        </span>
        <span
          className="mx-2 is-clickable"
          onClick={() => handleBillingAction(value)}
        >
          <BillingIcon />
        </span>
        <span
          className="mx-2 is-clickable"
          onClick={() => handleBillingHistoryAction(value)}
        >
          <BillingHistoryIcon />
        </span>
        <span
          className="mx-2 is-clickable"
          onClick={() => handleDeleteAction(value)}
        >
          <DeleteIcon />
        </span>
      </React.Fragment>
    );
  };

  const frameworkComponents = {
    actionBtnCellRenderer: ActionButtonRenderer,
  };

  return (
    <React.Fragment>
      <Table
        rowData={data}
        columnDefs={columnDefs}
        defaultColDef={defaultColDef}
        paginationAutoPageSize={true}
        pagination={true}
        frameworkComponents={frameworkComponents}
      />
      {confirmContainer}
    </React.Fragment>
  );
};
