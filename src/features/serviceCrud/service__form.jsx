import React, { useEffect } from "react";
import Joi from "joi";
import _ from "lodash";
import { toast } from "react-toastify";
import { FormCard } from "core/cards/formCard";
import { DateField, EditTextField, RadioButtonField, SelectContainer, TextAreaField } from "core/components/inputWithAccessControl";
import { Modal } from "core/components/modal";
import { useLoading } from "core/hooks/loadingHook";
import { useSubmitForm } from "core/hooks/submitFormHook";
import { useMasterListState } from "features/masterList/masterListProvider";
import { addDefaultOption, formatArrayToOptions } from "core/utility/util";
import { addService, editService } from "./serviceCrudService";

export const ServiceForm = ({ open, onClose, selectedData, onUpdateSuccess }) => {
  // console.log("service selected:",selectedData)
  const { loaderContainer, startLoading, stopLoading } = useLoading();
  // Node Type Options
//product Options

const { productTypeList, companyList } = useMasterListState();
// console.log("product type list:",productTypeList)
// const productTypesNames = _.map(productTypeList, "name");
// const productTypeOptions = addDefaultOption(
//   formatArrayToOptions(productTypesNames)
// );
// const handleProductTypeChange = (key, value) => {
//   handleInputChange(key, value);
//   const obj = _.find(productTypeList, { name: value });

//   handleInputChange("productId", obj["id"]);
// };


const sortedProductList = _.sortBy(productTypeList, [ "name"]);
const productOptions = _.map(sortedProductList, (productType) => {
  const {  name } = productType;
  return { label: `${name}`};
});
const productTypeOptions = addDefaultOption(productOptions);


  const schema = Joi.object({
    id: Joi.number().label("Id").positive().allow(0),
    productType: Joi.string().label("Product Name").required(),
    remark: Joi.string().trim().label("Remarks").allow(''),
    companyId: Joi.string().trim().label("Company Name").required(),
    expDate: Joi.date().label("Expiry Date").required(),    
    totalNodes: Joi.number().positive().integer().label("Total Nodes").when('productType', {
      is: 'platform license',
      then: Joi.required(),
      otherwise: Joi.optional()
    }),
    totalParameters: Joi.number().positive().integer().label("Total Parameters").when('productType', {
      is: 'platform license',
      then: Joi.required(),
      otherwise: Joi.optional()
    }),
    retentionPeriod: Joi.number().positive().integer().label("Retention Period").when('productType', {
      is: 'platform license',
      then: Joi.required(),
      otherwise: Joi.optional()
    }),
    cameraConnected: Joi.string().valid('yes', 'no').label("Camera Connected").when('productType', {
      is: 'platform license',
      then: Joi.required(),
      otherwise: Joi.optional()
    }),
    thirdPartyConnectivity: Joi.string().valid('yes', 'no').label("Third Party Connectivity").when('productType', {
      is: 'platform license',
      then: Joi.required(),
      otherwise: Joi.optional()
    }),
  });

  const accessToEdit = true;
  const submitCallback = async (event) => {
    const { id, productType, totalNodes,totalParameters,retentionPeriod,remark,expDate,companyId,
      cameraConnected,
      thirdPartyConnectivity } = inputs;
    // console.log("inputs:",inputs)
    const companyObj = _.find(companyList, {id: _.toInteger(companyId)});

    // console.log("company obj:",companyObj)
    try {
      startLoading();
      let body = {}
      if (_.toLower(productType) == "platform license"){
       body = { id, productType, totalNodes ,totalParameters,retentionPeriod,remark,expDate,companyId,
        cameraConnected: cameraConnected.toLowerCase() === "yes" ? true : false, 
        thirdPartyConnectivity: thirdPartyConnectivity.toLowerCase() === "yes" ? true : false,
              companyName: `${companyObj["name"]} - ${companyObj["plotNo"]}`
       };
       
      }else if(_.toLower(productType) == "amc"){
        
       }
 
   
      // console.log(body);
      if (id === null || id === undefined || id === "" || id === "0" || id === 0) {
        const { data: product } = await addService(body);
        onUpdateSuccess(product);
        toast.success("Service Added Successfully");
      } else {
        const { data: product } = await editService(id, body);
        onUpdateSuccess(product);
        toast.success("Service Edited Successfully");
      }
      stopLoading();
    } catch (error) {
      // console.log("error:",error)
      toast.error(error);
      stopLoading();
    }
    onClose();
  };


  const {
    inputs,
    errors,
    handleInputChange,
    handleSubmit,
    resetInput,
    additionalValidation,
  } = useSubmitForm(schema, submitCallback);

//company options
const sortedCompanyList = _.sortBy(companyList, [ "name"]);
const companyOptions = _.map(sortedCompanyList, (site) => {
  const {  name, plotNo, id } = site;
  return { label: `${name} - ${plotNo}`, value: id};
});
const companiesOptions = addDefaultOption(companyOptions);

  useEffect(() => {
    if (selectedData) {
      handleInputChange("id", selectedData["id"]);
      handleInputChange("productType", selectedData["productType"]);
      handleInputChange("totalNodes", selectedData["totalNodes"]);
      handleInputChange("totalParameters", selectedData["totalParameters"]);
      handleInputChange("retentionPeriod", selectedData["retentionPeriod"]);
      handleInputChange("remark", selectedData["remark"]);
      handleInputChange("expDate", selectedData["expDate"]);
      handleInputChange("companyId", selectedData["companyId"]);
      handleInputChange("cameraConnected", selectedData["cameraConnected"]);
      handleInputChange("thirdPartyConnectivity", selectedData["thirdPartyConnectivity"]);
      
    } else {
      resetInput();
      handleInputChange("id", 0);
    }
  }, [selectedData]);
  const today = new Date();
  const maxDate = new Date(today);
  maxDate.setFullYear(maxDate.getFullYear() + 20);

  const FormHtml = (
    
    <FormCard
      formName={"Add/Update Product"}
      onSubmit={handleSubmit}
      onFormReset={resetInput}
      submitAccess={accessToEdit}
    >
      <div className="columns is-multiline">
        <div className="column is-one-quarter">
          <EditTextField
            identifier="id"
            labelName="Id"
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
          />
        </div>
        <div className="column is-one-quarter">
          <SelectContainer
            identifier="companyId"
            labelName="Company Name"
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
            options={companiesOptions}
          />
        </div>
        <div className="column is-one-quarter">
          <SelectContainer
            identifier="productType"
            labelName="Product Type"
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
            options={productTypeOptions}
            editAccess={true}
          />
        </div>
        <div className="column is-one-quarter">
          <DateField
            identifier="expDate"
            labelName="Expiry Date (yyyy-mm-dd)"
            handleInputChange={handleInputChange}
            min={today}
            max={maxDate}
            inputs={inputs}
            errors={errors}
            editAccess={true}
          />
        </div>
        {_.toLower(inputs.productType) === 'platform license' && (
          <>
         
          <div className="column is-one-fifth">
            <EditTextField
              identifier="totalNodes"
              labelName="Total Nodes"
              handleInputChange={handleInputChange}
              inputs={inputs}
              errors={errors}
              editAccess={true}
            />
          </div>
           <div className="column is-one-fifth">
           <EditTextField
             identifier="totalParameters"
             labelName="Total Parameters"
             handleInputChange={handleInputChange}
             inputs={inputs}
             errors={errors}
             editAccess={true}
           />
         </div>
         <div className="column is-one-fifth">
           <EditTextField
             identifier="retentionPeriod"
             labelName="Retention Period In Months"
             handleInputChange={handleInputChange}
             inputs={inputs}
             errors={errors}
             editAccess={true}
           />
         </div>
         <div className="column is-one-fifth">
         <RadioButtonField
          identifier="cameraConnected"
          labelName="Camera Connected"
          handleInputChange={handleInputChange}
          inputs={inputs}
          errors={errors}
          editAccess={true}
          type="radio"
          required={true}
          options={[
            { value: "yes", label: "Yes" },
            { value: "no", label: "No" }
          ]}
          />
        </div>
        <div className="column is-one-fifth">
         <RadioButtonField
          identifier="thirdPartyConnectivity"
          labelName="Third Party Connectivity"
          handleInputChange={handleInputChange}
          inputs={inputs}
          errors={errors}
          editAccess={true}
          type="radio"
          required={true}
          options={[
            { value: "yes", label: "Yes" },
            { value: "no", label: "No" }
          ]}
          />
        </div>
        
         </>
        )}
        <div className="column is-full">
           <TextAreaField
             identifier="remark"
             labelName="Remarks"
             handleInputChange={handleInputChange}
             required={false}
             inputs={inputs}
             errors={errors}
             editAccess={true}
             rows={2}
           />
         </div>
      </div>
    </FormCard>
  );

  return (
    <Modal open={open} onClose={onClose} width="full-width">
      <div>{FormHtml}</div>
      {loaderContainer}
    </Modal>
  );
};
