import config from "config.json";
import http from "core/service/httpService";

export function getAssignedFeatures() {
  return http.get(`${config.apiEndpoint}/features/assign/me`);
}
