import React, { useState } from "react";
import _ from "lodash";
import { useModal } from "core/hooks/modalHook";
import { toast } from "react-toastify";
import { deleteBilling } from "./billingCrudService";
import { BillingSearch } from "./billing_search";
import { BillingTable } from "./billing_table";
export const BillingCrud = () => {
  const [companies, setCompanies] = useState([]);
  const [action, setAction] = useState("");
  const [selectedInput, setSelectedInput] = useState();
  const [selectedId, setSelectedId] = useState(0);
  const handleSearchResults = (output) => {
    // console.log(output);
    setCompanies(output);
  };

  // Search Options

  // Add Action
  const handleAddNew = () => {
   toast.warning("This Feature Not Implimented")
  };

  // Delete Action
  const handleDelete = async (data) => {
    try {
      setAction("DELETE");
      setSelectedId(data["id"]);
      setSelectedInput(data);
      const { data: company } = await deleteBilling(data["id"]);
      toast.success("company deleted successfully");
      const newList = [...companies];
      _.remove(newList, function (n) {
        console.log("Checking " + data["id"] + " " + n["id"]);
        return n["id"] == data["id"];
      });
      // console.log(newList);
      setCompanies(newList);
    } catch (error) {
      console.log(error);
      toast.error("Error deleting company");
    }
  };



  const [ openFormModal] = useModal(false);

  return (
    <React.Fragment>
      <BillingSearch
        onSearchResults={handleSearchResults}
        onAddNew={handleAddNew}
      />
      <BillingTable
        data={companies}
        onDelete={handleDelete}
      />
    
    </React.Fragment>
  );
};
