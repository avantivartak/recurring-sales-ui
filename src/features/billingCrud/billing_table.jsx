import { Modal } from "core/components/modal";
import { DeleteIcon, EditIcon } from "core/components/svgIcon";
import { Table } from "core/components/table";
import { useConfirmBox } from "core/hooks/confirmHook";
import _ from "lodash";
import React from "react";

export const BillingTable = ({ data, onEdit, onDelete }) => {
  // Edit action

  const handleEditAction = (value) => {
    const companyData = _.pick(value, [
     "id",
      "expectedAmount",
      "InvoiceAmount",
      "InvoiceNo",
      "InvoiceDate"
      ]);
    onEdit(companyData);

  };
  // Delete Action

  const options = { onSuccess: onDelete };
  const { confirmContainer, openConfirmBox, setSelectedInput } =
    useConfirmBox(options);

  const handleDeleteAction = (value) => {
    setSelectedInput(value);
    openConfirmBox();
  };
 
  const defaultColDef = {
    sortable: true,
    resizable: true,
    filter: true,
    floatingFilter: true,
    flex: 1,
    minWidth: 150,
  };

  const columnDefs = [
    {
      headerName: "Billing History",
      children: [
        {
          headerName: "Id",
          field: "id",
        },
        {
          headerName: "Expected Amount",
          field: "expectedAmount",
          maxWidth: 550,
        },
        {
          headerName: "Invoice Amount",
          field: "InvoiceAmount",
          maxWidth: 550,
        },
        {
            headerName: "Invoice No.",
            field: "InvoiceNo",
            maxWidth: 550,
          },
          {
            headerName: "Invoice Date",
            field: "InvoiceDate",
            maxWidth: 550,
          },
        
        {
          headerName: "Details",
          valueGetter: function (params) {
            return params.data;
          },
          cellRenderer: "actionBtnCellRenderer",
          sortable: false,
          filter: false,
          minWidth: 300,
        },
      ],
    },
  ];

  const ActionButtonRenderer = ({ value }) => {
    return (
      <React.Fragment>
        <span
          className="mx-2 is-clickable"
          onClick={() => handleEditAction(value)}
        >
          <EditIcon />
        </span>
        <span
          className="mx-2 is-clickable"
          onClick={() => handleDeleteAction(value)}
        >
          <DeleteIcon />
        </span>
      </React.Fragment>
    );
  };

  const frameworkComponents = {
    actionBtnCellRenderer: ActionButtonRenderer,
  };

  return (
    <React.Fragment>

      <Table
        rowData={data}
        columnDefs={columnDefs}
        defaultColDef={defaultColDef}
        paginationAutoPageSize={true}
        pagination={true}
        frameworkComponents={frameworkComponents}
      />
      {confirmContainer}
    </React.Fragment>
  );
};
