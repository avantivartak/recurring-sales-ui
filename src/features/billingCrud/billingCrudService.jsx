import config from "config.json";
import http from "core/service/httpService";

export function searchBilling(body) {
  return http.get(`${config.apiEndpoint}/billings/search`, {
    params: body,
  });
}

export function findAll(body) {
  return http.get(`${config.apiEndpoint}/billings`, {
    params: body,
  });
}

// export function addBilling(body) {
//   return http.filePost(`${config.apiEndpoint}/companies`, body);
// }

export function addBilling(body) {
  return http.post(`${config.apiEndpoint}/billings`, body);
}

export function editBilling(id, body) {
  return http.put(`${config.apiEndpoint}/billings/${id}`, body);
}

export function deleteBilling(id) {
  return http.delete(`${config.apiEndpoint}/billings/${id}`);
}
