import { FormCard } from "core/cards/formCard";
import { MultiCheckboxContainer } from "core/components/inputWithAccessControl";
import { Modal } from "core/components/modal";
import { formatArrayToOptions } from "core/utility/util";
import { useLoading } from "core/hooks/loadingHook";
import { useSubmitForm } from "core/hooks/submitFormHook";
import Joi from "joi";
import _ from "lodash";
import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { fetchAllSites } from "./userCrudService";
import { assignSites } from "./userCrudService";
import { getAssignedSitesByUserId } from "./userCrudService";
import { useMasterListState } from "features/masterList/masterListProvider";

export const SiteForm = ({ open, onClose, selectedData = {} }) => {
  const { id } = selectedData;
  const [associatedSites, setAssociatedSites] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const { data: sites } = await getAssignedSitesByUserId(id);
        // console.log(sites);
        const siteIds = _.map(sites, (f) => {
          return _.toString(f["id"]);
        });
        setAssociatedSites(siteIds);
        // setSites(dummy);
      } catch (ex) {
        console.log(ex);
        toast.error("Error while fetching site data");
      }
    };
    if (id) {
      fetchData();
    }
  }, [id]);
  //   console.log(selectedData);
  // console.log(associatedSites);
  const schema = Joi.object({
    sites: Joi.array().items(Joi.string().trim()),
  });
  const { loaderContainer, startLoading, stopLoading } = useLoading();
  const { siteList } = useMasterListState();

  // Site Options
  const sortedSiteList = _.sortBy(siteList, ["companyName", "name"]);
  const siteOptions = _.map(sortedSiteList, (site) => {
    const { id, name, companyName } = site;
    return { label: `${companyName} - ${name}`, value: _.toString(id) };
  });
  // console.log(siteOptions);
  const submitCallback = async (event) => {
    try {
      startLoading();
      // console.log(inputs);
      const sitesBody = [];
      _.forEach(inputs["sites"], function (site) {
        sitesBody.push(_.toInteger(site));
      });
      const body = {
        userId: id,
        siteIds: sitesBody,
      };
      // console.log(body);
      const { data: sites } = await assignSites(id, body);
      toast.success("Sites updated successfully.");
      // toast.warn("Functionality not implemented.");
      stopLoading();
      onClose();
      //   onUpdateSuccess(updatedDisposalData);
    } catch (error) {
      console.log(error);
      toast.error(
        "Error updating site. Please refer console for detailed error"
      );
      stopLoading();
      onClose();
    }
  };
  const defaultInputs = {};
  useEffect(() => {
    if (associatedSites) {
      handleInputChange("sites", associatedSites);
    }
  }, [associatedSites]);
  const { inputs, errors, handleInputChange, handleSubmit, resetInput } =
    useSubmitForm(schema, submitCallback, defaultInputs);
  // console.log(inputs);
  const WrappingContainer = ({ children }) => (
    <div className="column is-one-third">{children}</div>
  );

  const FormHtml = (
    <FormCard
      formName={"Site Form"}
      onSubmit={handleSubmit}
      onFormReset={resetInput}
      submitAccess={true}
    >
      <div className="columns is-multiline">
        <MultiCheckboxContainer
          identifier={"sites"}
          labelName={"Sites"}
          handleInputChange={handleInputChange}
          inputs={inputs}
          errors={errors}
          editAccess={true}
          options={siteOptions}
          wrapper={WrappingContainer}
        />
      </div>
      {/* /.columns */}
    </FormCard>
  );

  return (
    <Modal open={open} onClose={onClose} width="full-width">
      <div>{FormHtml}</div>
      {loaderContainer}
    </Modal>
  );
};
