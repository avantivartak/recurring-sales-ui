import config from "config.json";
import http from "core/service/httpService";

export function searchUser(body) {
  return http.get(`${config.apiEndpoint}/users/search`, {
    params: body,
  });
}

export function findAll(body) {
  return http.get(`${config.apiEndpoint}/users`, {
    params: body,
  });
}

export function addUser(body) {
  return http.post(`${config.apiEndpoint}/users`, body);
}

export function editUser(id, body) {
  return http.put(`${config.apiEndpoint}/users/${id}`, body);
}

export function deleteUser(id) {
  return http.delete(`${config.apiEndpoint}/users/${id}`);
}

export function fetchAllFeatures() {
  return http.get(`${config.apiEndpoint}/features`);
}
export function assignFeatures(id, body) {
  return http.post(`${config.apiEndpoint}/features/assign`, body);
}

export function getAssignedFeaturesByUserId(userId) {
  return http.get(`${config.apiEndpoint}/features/assign/${userId}`);
}

export function assignSites(id, body) {
  return http.post(`${config.apiEndpoint}/sites/assign`, body);
}

export function getAssignedSitesByUserId(userId) {
  return http.get(`${config.apiEndpoint}/sites/assign/${userId}`);
}

export function findNodesBySiteId(body) {
  return http.get(`${config.apiEndpoint}/nodes/site/${body.name}`);
}

export function assignNodes(id, body) {
  return http.post(`${config.apiEndpoint}/nodes/assign`, body);
}

export function getAssignedNodesByUserId(userId) {
  return http.get(`${config.apiEndpoint}/nodes/assigned/${userId}`);
}

export function assignDashboards(id, body) {
  return http.post(`${config.apiEndpoint}/analytics/assign`, body);
}

export function getAssignedDashboardsByUserId(userId) {
  return http.get(`${config.apiEndpoint}/analytics/assign/${userId}`);
}
