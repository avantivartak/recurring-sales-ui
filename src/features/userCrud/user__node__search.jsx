import React from "react";
import Joi from "joi";
import { SearchAndAddCard } from "core/cards/searchAndAddCard";
import { useLoading } from "core/hooks/loadingHook";
import { toast } from "react-toastify";
import _ from "lodash";
import { useSubmitForm } from "core/hooks/submitFormHook";
import { SelectContainer } from "core/components/inputWithAccessControl";
import { useMasterListState } from "features/masterList/masterListProvider";
import { addDefaultOption } from "core/utility/util";
import { findNodesBySiteId } from "./userCrudService";
import { SearchableSelectContainer } from "core/components/inputWithAccessControl";

export const NodeSearch = ({ onSearchResults }) => {
  const { companyList, siteList } = useMasterListState();

  // Site Options
  const sortedSiteList = _.sortBy(siteList, ["companyName", "name"]);
  const siteOptions = _.map(sortedSiteList, (site) => {
    const { id, name, companyName } = site;
    return { label: `${companyName} - ${name}`, value: _.toString(id) };
  });
  // const option = { label: value, value: value };
  const siteNameOptions = addDefaultOption(siteOptions);

  const { loaderContainer, startLoading, stopLoading } = useLoading();
  const schema = Joi.object({
    name: Joi.string().trim().label("Node Name").allow(""),
  });
  const submitCallback = async (event) => {
    const { name } = inputs;
    try {
      startLoading();
      const body = {
        name: _.isNil(name) ? "" : name,
      };
      // console.log(nodeName);
      // console.log(body);
      const { data: nodes } = await findNodesBySiteId(body);
      onSearchResults(nodes);
      //   console.log(nodes);
      if (nodes.length > 0) {
        toast.success("Data fetched");
      } else {
        toast.warn("No data found");
      }

      stopLoading();
    } catch (error) {
      console.log(error);
      if (error.response && error.response.status === 400) {
        toast.warn("No data found");
      } else {
        toast.error("Error fetching node data");
      }
      stopLoading();
    }
  };
  const defaultInputs = {};
  const {
    inputs,
    errors,
    handleInputChange,
    handleSubmit,
    resetInput,
    additionalValidation,
  } = useSubmitForm(schema, submitCallback, defaultInputs);

  return (
    <React.Fragment>
      <SearchAndAddCard
        formName={"Search Node"}
        onSubmit={handleSubmit}
        submitAccess={true}
        submitText={"Search"}
      >
        <div className="columns is-multiline">
          <div className="column is-one-third">
            <SearchableSelectContainer
              identifier="name"
              labelName="Site Name"
              handleInputChange={handleInputChange}
              editAccess={true}
              inputs={inputs}
              errors={errors}
              options={siteNameOptions}
            />
          </div>
          {/* /.column */}
        </div>
        {/* /.columns */}
      </SearchAndAddCard>
      {loaderContainer}
    </React.Fragment>
  );
};
