import { FormCard } from "core/cards/formCard";
import { MultiCheckboxContainer } from "core/components/inputWithAccessControl";
import { Modal } from "core/components/modal";
import { useLoading } from "core/hooks/loadingHook";
import { useSubmitForm } from "core/hooks/submitFormHook";
import Joi from "joi";
import _ from "lodash";
import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { findAll } from "features/analyticsCrud/analyticsCrudService";
import { assignDashboards } from "./userCrudService";
import { getAssignedDashboardsByUserId } from "./userCrudService";

export const DashboardForm = ({ open, onClose, selectedData = {} }) => {
    const { id } = selectedData;
    const [associatedDashboard, setAssociatedDashboard] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const { data: features } = await getAssignedDashboardsByUserId(id);
                const dashboardIds = _.map(features, (f) => {
                    return _.toString(f["id"]);
                });
                setAssociatedDashboard(dashboardIds);

                // setFeatures(dummy);
            } catch (ex) {
                console.log(ex);
                toast.error("Error while fetching dashboard data");
            }
        };
        if (id) {
            fetchData();
        }
    }, [id]);
    //   console.log(selectedData);
    // console.log(associatedFeatures);
    const schema = Joi.object({
        dashboards: Joi.array().items(Joi.number().positive().integer()),
    });
    const { loaderContainer, startLoading, stopLoading } = useLoading();
    const [dashboardOptions, setDashboardOptions] = useState([]);
    useEffect(() => {
        const fetchData = async () => {
            // const user = getUser();
            // console.log(user);
            const { data: dashboards } = await findAll();
            //   console.log(features);
            const sortedDashboards = _.sortedUniqBy(
                _.sortBy(dashboards, ["firstName"]),
                "firstName"
            );

            //   const featureNames = _.uniq(_.map(sortedFeatures, "name"));
            //   const featureOptions = formatArrayToOptions(featureNames);
            const dashboardOptions = [];
            _.forEach(sortedDashboards, (f) => {
                dashboardOptions.push({ value: _.toString(f.id), label: f.firstName });
            });
            setDashboardOptions(dashboardOptions);
        };
        fetchData();
    }, []);
    const submitCallback = async (event) => {
        try {
            startLoading();
            // console.log(inputs);
            const dashboardBody = [];
            _.forEach(inputs["dashboards"], function (dashboard) {
                dashboardBody.push({ id: _.toInteger(dashboard) });
            });
            const body = {
                userId: id,
                dashboards: dashboardBody,
            };
            //   console.log(body);
            const { data: dashboards } = await assignDashboards(id, body);
            toast.success("Dashboard updated successfully.");
            stopLoading();
            onClose();
            //   onUpdateSuccess(updatedDisposalData);
        } catch (error) {
            console.log(error);
            toast.error(
                "Error updating dashboards. Please refer console for detailed error"
            );
            stopLoading();
            onClose();
        }
    };
    const defaultInputs = {};
    useEffect(() => {
        if (associatedDashboard) {
            handleInputChange("dashboards", associatedDashboard);
        }
    }, [associatedDashboard]);
    const { inputs, errors, handleInputChange, handleSubmit, resetInput } =
        useSubmitForm(schema, submitCallback, defaultInputs);
    // console.log(inputs);
    const WrappingContainer = ({ children }) => (
        <div className="column is-one-third">{children}</div>
    );

    const FormHtml = (
        <FormCard
            formName={"Dashboard Form"}
            onSubmit={handleSubmit}
            onFormReset={resetInput}
            submitAccess={true}
        >
            <div className="columns is-multiline">
                <MultiCheckboxContainer
                    identifier={"dashboards"}
                    labelName={"Dashboard"}
                    handleInputChange={handleInputChange}
                    inputs={inputs}
                    errors={errors}
                    editAccess={true}
                    options={dashboardOptions}
                    wrapper={WrappingContainer}
                />
            </div>
            {/* /.columns */}
        </FormCard>
    );

    return (
        <Modal open={open} onClose={onClose} width="full-width">
            <div>{FormHtml}</div>
            {loaderContainer}
        </Modal>
    );
};
