import config from "config.json";
import http from "core/service/httpService";

export function getAssignedNodes() {
  return http.get(`${config.apiEndpoint}/nodes/me`);
}

export function getLoggedInUserDetails() {
  return http.get(`${config.apiEndpoint}/users/me`);
}

export function getAssignedProcessTags() {
  return http.get(`${config.apiEndpoint}/tags/assign/me`);
}

export function getAllCompanies() {
  return http.get(`${config.apiEndpoint}/companies`);
}
export function getAllProductTypes() {
  return http.get(`${config.apiEndpoint}/productTypes`);
}
export function getAllVendors() {
  return http.get(`${config.apiEndpoint}/vendors`);
}
// export function getAllSites() {
//   return http.get(`${config.apiEndpoint}/sites`);
// }

// export function getAllNodeTypes() {
//   return http.get(`${config.apiEndpoint}/nodeTypes`);
// }

// export function getAllParameters() {
//   return http.get(`${config.apiEndpoint}/parameters`);
// }
