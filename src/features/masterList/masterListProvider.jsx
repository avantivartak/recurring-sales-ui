import React, { useEffect, useState } from "react";

import _ from "lodash";
import {
  getAllProductTypes,
  getAllVendors,
} from "./masterListService";
import dummy from "./dummyData.json";
import { getAllCompanies } from "./masterListService";
const MasterlistStateContext = React.createContext();

function MasterlistProvider({ children }) {
  // const [masterList, setMasterList] = useState({
  //   sites: [],
  //   user: {},
  //   company: {},
  // });
  const [masterList, setMasterList] = useState({
    user: {},
    company: {},
    productTypeList:[],
    vendorsList:[],
    companyList: [],
    siteList: [],
    nodeTypeList: [],
    parameterList: [],
  });
  useEffect(() => {
    const fetchData = async () => {
      const newMasterList = { ...masterList };
      // console.log(sites);

      //   const { data: company } = await getLoggedInCompanyDetails();
      //   newMasterList["company"] = company;
      try {
        // const { data: user } = await getLoggedInUserDetails();
        const user = dummy["user"];
        newMasterList["user"] = user;
      } catch (error) {
        console.log(error);
        // toast.error("Unable to fetch user data");
      }

      // Fetching all companies
      try {
        const { data: companies } = await getAllCompanies();
        newMasterList["companyList"] = companies;
      } catch (error) {
        console.log(error);
        // toast.error("Unable to fetch user data");
      }

      // Fetching all product type
      try {
        const { data: productTypes } = await getAllProductTypes();
        newMasterList["productTypeList"] = productTypes;
      } catch (error) {
        console.log(error);
        // toast.error("Unable to fetch user data");
      }
      //fetching all vendors
      try {
        const { data: vendors } = await getAllVendors();
        newMasterList["vendorsList"] = vendors;
      } catch (error) {
        console.log(error);
        // toast.error("Unable to fetch user data");
      }
      // Fetching all sites
      // try {
      //   const { data: sites } = await getAllSites();
      //   newMasterList["siteList"] = sites;
      // } catch (error) {
      //   console.log(error);
      //   // toast.error("Unable to fetch user data");
      // }

      // Fetching all node types
      // try {
      //   const { data: nodeTypes } = await getAllNodeTypes();
      //   newMasterList["nodeTypeList"] = nodeTypes;
      // } catch (error) {
      //   console.log(error);
      //   // toast.error("Unable to fetch user data");
      // }

      // Fetching all sites
      // try {
      //   const { data: parameters } = await getAllParameters();
      //   newMasterList["parameterList"] = parameters;
      // } catch (error) {
      //   console.log(error);
      //   // toast.error("Unable to fetch user data");
      // }

      // try {
      //   // const { data: processTags } = await getAssignedProcessTags();
      //   const processTags = dummy["processTags"];
      //   newMasterList["processTags"] = processTags;
      // } catch (error) {
      //   console.log(error);
      //   // toast.error("Unable to fetch process tags");
      // }

      // try {
      //   const { data: nodes } = await getAssignedNodes();
      //   // const nodes = dummy["nodes"];

      //   newMasterList["nodes"] = nodes;
      //   console.log(nodes);

      //   const nodeTypes = _.uniq(_.map(nodes, "nodeType"));
      //   newMasterList["nodeTypes"] = nodeTypes;

      //   const nodeTags = _.uniq(_.map(nodes, "tagName"));
      //   newMasterList["nodeTags"] = nodeTags;
      // } catch (error) {
      //   console.log(error);
      //   // toast.error("Unable to fetch node data");
      // }
      setMasterList(newMasterList);
    };
    if (masterList["productTypeList"].length == 0 || masterList["vendorsList"].length == 0 || masterList["companyList"].length == 0) {
      fetchData();
    }
  }, []);
  return (
    <MasterlistStateContext.Provider value={masterList}>
      {children}
    </MasterlistStateContext.Provider>
  );
}

function useMasterListState() {
  const context = React.useContext(MasterlistStateContext);
  if (context === undefined) {
    throw new Error(
      "useMasterListState must be used within a MasterListProvider"
    );
  }
  return context;
}

export { MasterlistProvider, useMasterListState };
