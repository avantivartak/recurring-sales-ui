import { DeleteIcon } from "core/components/svgIcon";
import { EditIcon } from "core/components/svgIcon";
import { Table } from "core/components/table";
import { useConfirmBox } from "core/hooks/confirmHook";
import _ from "lodash";
import React, { useState } from "react";

export const AnalyticsTable = ({ data, onEdit, onDelete }) => {
    // Edit action

    const handleEditAction = (value) => {
        const dashboardData = _.pick(value, [
            "id",
            "supersetId",
            "type",
            "clause",
            "dataset",
            "firstName",
            "lastName",
            "userName",

        ]);
        onEdit(dashboardData);
    };

    // Delete Action

    const options = { onSuccess: onDelete };
    const { confirmContainer, openConfirmBox, setSelectedInput } =
        useConfirmBox(options);

    const handleDeleteAction = (value) => {
        setSelectedInput(value);
        openConfirmBox();
    };

    const defaultColDef = {
        sortable: true,
        resizable: true,
        filter: true,
        floatingFilter: true,
        flex: 1,
        minWidth: 150,
    };

    const columnDefs = [
        {
            headerName: "Analytics",
            children: [
                {
                    headerName: "Id",
                    field: "id",
                    maxWidth: 500,
                },

                {
                    headerName: "Superset Id",
                    field: "supersetId",
                    maxWidth: 120,
                },
                {
                    headerName: "Type",
                    field: "type",
                    maxWidth: 150,
                },
                {
                    headerName: "Clause",
                    field: "clause",
                    minWidth: 150,
                },
                {
                    headerName: "Dataset",
                    field: "dataset",
                    minWidth: 150,
                },
                {
                    headerName: "First Name",
                    field: "firstName",
                    minWidth: 150,
                },
                {
                    headerName: "Last Name",
                    field: "lastName",
                    minWidth: 150,
                },
                {
                    headerName: "User Name",
                    field: "userName",
                    minWidth: 150,
                },
                {
                    headerName: "Details",
                    valueGetter: function (params) {
                        return params.data;
                    },
                    cellRenderer: "actionBtnCellRenderer",
                    sortable: false,
                    filter: false,
                    minWidth: 300,
                },
            ],
        },
    ];

    const ActionButtonRenderer = ({ value }) => {
        return (
            <React.Fragment>
                <span
                    className="mx-2 is-clickable"
                    title="Edit"
                    onClick={() => handleEditAction(value)}
                >
                    <EditIcon />
                </span>
                <span
                    title="Delete"
                    className="mx-2 is-clickable"
                    onClick={() => handleDeleteAction(value)}
                >
                    <DeleteIcon />
                </span>

            </React.Fragment>
        );
    };

    const frameworkComponents = {
        actionBtnCellRenderer: ActionButtonRenderer,
    };

    return (
        <React.Fragment>
            <Table
                rowData={data}
                columnDefs={columnDefs}
                defaultColDef={defaultColDef}
                paginationAutoPageSize={true}
                pagination={true}
                frameworkComponents={frameworkComponents}
            />
            {confirmContainer}
        </React.Fragment>
    );
};
