import { FormCard } from "core/cards/formCard";
import { EditTextField } from "core/components/inputWithAccessControl";
import { Modal } from "core/components/modal";
import { useLoading } from "core/hooks/loadingHook";
import { useSubmitForm } from "core/hooks/submitFormHook";
import Joi from "joi";
import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import _ from "lodash";

import { addDashboard, editDashboard } from "./analyticsCrudService";



export const AnalyticsForm = ({
    open,
    onClose,
    selectedData,
    onUpdateSuccess,
}) => {
    const { loaderContainer, startLoading, stopLoading } = useLoading();

    const schema = Joi.object({
        id: Joi.number().label("Id").allow(""),
        supersetId: Joi.string().trim().label("Superset Id"),
        type: Joi.string().trim().label("Type"),
        clause: Joi.string().trim().label("Clause"),
        dataset: Joi.number().positive().label("dataset"),
        firstName: Joi.string().trim().label("First Name"),
        lastName: Joi.string().trim().label("Last Name").required(),
        userName: Joi.string().trim().label("User Name").required(),
    });
    const accessToEdit = true;
    const submitCallback = async (event) => {
        // console.log(inputs);

        const { id, supersetId, type, clause, dataset, firstName, lastName, userName } = inputs;
        try {
            startLoading();

            const body = {
                id,
                supersetId,
                type,
                clause,
                dataset,
                firstName,
                lastName,
                userName
            };
            // console.log(body);
            if (
                id === null ||
                id === undefined ||
                id === "" ||
                id === "0" ||
                id === 0
            ) {
                const { data: dashboard } = await addDashboard(body);
                onUpdateSuccess(dashboard);
                toast.success("Dashboard Added Successfully");
            } else {
                const { data: dashboard } = await editDashboard(id, body);
                onUpdateSuccess(dashboard);
                console.log(dashboard);
                toast.success("Dashboard Edited Successfully");
            }
            //   console.log(companies);
            stopLoading();
        } catch (error) {
            console.log(error);
            toast.error("Error adding / editing dashboard details");
            stopLoading();
        }
        onClose();
    };
    let defaultInputs = { id: 0 };
    useEffect(() => {
        if (selectedData) {
            console.log(selectedData);
            handleInputChange("id", selectedData["id"]);
            handleInputChange("supersetId", selectedData["supersetId"]);
            handleInputChange("type", selectedData["type"]);
            handleInputChange("clause", selectedData["clause"]);
            handleInputChange("dataset", selectedData["dataset"]);
            handleInputChange("firstName", selectedData["firstName"]);
            handleInputChange("lastName", selectedData["lastName"]);
            handleInputChange("userName", selectedData["userName"]);


        } else {
            resetInput();
            handleInputChange("id", 0);
        }
    }, [selectedData]);
    const {
        inputs,
        errors,
        handleInputChange,
        handleSubmit,
        resetInput,
        additionalValidation,
    } = useSubmitForm(schema, submitCallback, defaultInputs);


    const FormHtml = (
        <FormCard
            formName={"Add/Update Dashboard"}
            onSubmit={handleSubmit}
            onFormReset={resetInput}
            submitAccess={accessToEdit}
        >
            <div className="columns is-multiline">
                <div className="column is-one-third ">
                    <EditTextField
                        identifier="id"
                        labelName="Id"
                        handleInputChange={handleInputChange}
                        inputs={inputs}
                        errors={errors}
                    />
                </div>
                <div className="column is-one-third ">
                    <EditTextField
                        identifier="supersetId"
                        labelName="Superset Id"
                        handleInputChange={handleInputChange}
                        inputs={inputs}
                        errors={errors}
                        editAccess={true}
                    />
                </div>


                <div className="column is-one-third ">
                    <EditTextField
                        identifier="type"
                        labelName="Type"
                        handleInputChange={handleInputChange}
                        inputs={inputs}
                        errors={errors}
                        editAccess={true}

                    />
                </div>
                <div className="column is-one-third">
                    <EditTextField
                        identifier="clause"
                        labelName="Clause"
                        handleInputChange={handleInputChange}
                        inputs={inputs}
                        errors={errors}
                        editAccess={true}
                    />
                </div>
                {/* /.column */}

                <div className="column is-one-third">
                    <EditTextField
                        identifier="dataset"
                        labelName="Dataset"
                        handleInputChange={handleInputChange}
                        inputs={inputs}
                        errors={errors}
                        editAccess={true}
                    />
                </div>


                <div className="column is-one-third">
                    <EditTextField
                        identifier="firstName"
                        labelName="First Name"
                        handleInputChange={handleInputChange}
                        inputs={inputs}
                        errors={errors}
                        editAccess={true}
                    />
                </div>

                {/* /.column */}

                <div className="column is-one-third">
                    <EditTextField
                        identifier="lastName"
                        labelName="Last Name"
                        handleInputChange={handleInputChange}
                        inputs={inputs}
                        errors={errors}
                        editAccess={true}
                    />
                </div>
                {/* /.column */}
                <div className="column is-one-third">
                    <EditTextField
                        identifier="userName"
                        labelName="User Name"
                        handleInputChange={handleInputChange}
                        inputs={inputs}
                        errors={errors}
                        editAccess={true}
                    />
                </div>
                {/* /.column */}
            </div>
            {/* /.columns */}
        </FormCard>
    );

    return (
        <Modal open={open} onClose={onClose} width="full-width">
            <div>{FormHtml}</div>
            {loaderContainer}
        </Modal>
    );
};
