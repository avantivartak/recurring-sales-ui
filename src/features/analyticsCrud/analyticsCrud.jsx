import React, { useState } from "react";
import _ from "lodash";
import { useModal } from "core/hooks/modalHook";
import { toast } from "react-toastify";
import { deleteDashboard } from "./analyticsCrudService";
import { AnalyticsSearch } from "./analytics__searchForm";
import { AnalyticsTable } from "./analytics__table";
import { AnalyticsForm } from "./analytics__form";

export const AnalyticsCrud = () => {
    const [analytics, setAnalytics] = useState([]);
    const [action, setAction] = useState("");
    const [selectedInput, setSelectedInput] = useState();
    const [selectedId, setSelectedId] = useState(0);
    const handleSearchResults = (output) => {
        setAnalytics(output);
    };

    // Add Action
    const handleAddNew = () => {
        setAction("ADD");
        setSelectedId(0);
        setSelectedInput({});
        openFormModal();
    };

    // Edit Action
    const handleEdit = (data) => {
        setAction("EDIT");

        setSelectedId(data["id"]);
        // console.log("DATA = ",data);
        setSelectedInput(data);
        console.log(selectedInput);
        openFormModal();
    };

    // Delete Action
    const handleDelete = async (data) => {
        try {
            setAction("DELETE");
            setSelectedId(data["id"]);
            setSelectedInput(data);
            const { data: id } = await deleteDashboard(data["id"]);
            toast.success("Dashboard deleted successfully");
            const newList = [...analytics];
            _.remove(newList, function (n) {
                return n["id"] == data["id"];
            });
            setAnalytics(newList);
        } catch (error) {
            console.log(error);
            toast.error("Error deleting dashboard");
        }
    };


    // Update on success
    const handleAddUpdateSuccess = (updatedData) => {
        if (action === "EDIT") {
            const index = _.findIndex(analytics, { id: selectedId });
            const updatedDataList = [...analytics];
            updatedDataList[index] = updatedData;
            setAnalytics(updatedDataList);
        } else if (action === "ADD") {
            const newList = [...analytics];
            newList.push(updatedData);
            setAnalytics(newList);
        }
    };

    const [formModal, openFormModal, closeFormModal] = useModal(false);

    return (
        <React.Fragment>
            <AnalyticsSearch
                onSearchResults={handleSearchResults}
                onAddNew={handleAddNew}
            />
            <AnalyticsTable
                data={analytics}
                onEdit={handleEdit}
                onDelete={handleDelete}
            />
            <AnalyticsForm
                open={formModal}
                onClose={closeFormModal}
                selectedData={selectedInput}
                onUpdateSuccess={handleAddUpdateSuccess}
            />
        </React.Fragment>
    );

};


