import React from "react";
import Joi from "joi";
import { SearchAndAddCard } from "core/cards/searchAndAddCard";
import { useLoading } from "core/hooks/loadingHook";
import { toast } from "react-toastify";
import _ from "lodash";
import { useSubmitForm } from "core/hooks/submitFormHook";
import { EditTextField } from "core/components/inputWithAccessControl";
import { findAll } from "./analyticsCrudService";

export const AnalyticsSearch = ({ onSearchResults, onAddNew }) => {
    //   const { companyList, siteList } = useMasterListState();

    //   // Site Options
    //   const sortedSiteList = _.sortBy(siteList, ["companyName", "name"]);
    //   const siteOptions = _.map(sortedSiteList, (site) => {
    //     const { id, name, companyName } = site;
    //     return { label: `${companyName} - ${name}`, value: _.toString(id) };
    //   });
    //   // const option = { label: value, value: value };
    //   const siteNameOptions = addDefaultOption(siteOptions);

    const { loaderContainer, startLoading, stopLoading } = useLoading();
    const schema = Joi.object({
        name: Joi.string().trim().label("Site Name").allow(""),
    });
    const submitCallback = async (event) => {
        const { name } = inputs;
        try {

            //     startLoading();
            //     const body = {
            //         name: _.isNil(name) ? "" : name,
            //     };
            //     // console.log(nodeName);
            //     // console.log("Search Camera = ",body);
            //     const { data: camera } = await searchAnalytics(body);
            //     onSearchResults(camera);
            //     // console.log("CAMERA = ",camera);
            //     if (camera.length > 0) {
            //         toast.success("Data fetched");
            //     } else {
            //         toast.warn("No data found");
            //     }

            stopLoading();
        } catch (error) {
            console.log(error);
            if (error.response && error.response.status === 400) {
                toast.warn("No data found");
            } else {
                toast.error("Error fetching node data");
            }
            stopLoading();
        }
    };
    const defaultInputs = {};
    const {
        inputs,
        errors,
        handleInputChange,
        handleSubmit,
        resetInput,
        additionalValidation,
    } = useSubmitForm(schema, submitCallback, defaultInputs);

    const handleFindAll = async () => {
        try {
            startLoading();
            // console.log(nodeName);
            // console.log(body);
            const { data: analytics } = await findAll();
            onSearchResults(analytics);
            // console.log(camera);
            if (analytics.length > 0) {
                toast.success("Data fetched");
            } else {
                toast.warn("No data found");
            }

            stopLoading();
        } catch (error) {
            console.log(error);
            if (error.response && error.response.status === 400) {
                toast.warn("No data found");
            } else {
                toast.error(
                    "Error fetching camera data. Please refer console for detailed error"
                );
            }
            stopLoading();
        }
    };

    const handleAddNew = () => {
        onAddNew();
    };

    return (
        <React.Fragment>
            <SearchAndAddCard
                formName={"Search Analytics"}
                onSubmit={handleSubmit}
                onFormReset={resetInput}
                onAddNew={handleAddNew}
                onFindAll={handleFindAll}
                submitAccess={true}
                submitText={"Search"}
            >
                <div className="columns is-multiline">
                    <div className="column is-one-third">
                        <EditTextField
                            identifier="name"
                            labelName="Dashboard Name"
                            handleInputChange={handleInputChange}
                            editAccess={true}
                            inputs={inputs}
                            errors={errors}
                        //   options={siteNameOptions}
                        />
                    </div>
                    {/* /.column */}
                </div>
                {/* /.columns */}
            </SearchAndAddCard>
            {loaderContainer}
        </React.Fragment>
    );
};
