import config from "config.json";
import http from "core/service/httpService";



export function findAll(body) {
    return http.get(`${config.apiEndpoint}/analytics`, {
        params: body,
    });
}


export function addDashboard(body) {
    return http.post(`${config.apiEndpoint}/analytics`, body);
}

export function editDashboard(id, body) {
    return http.put(`${config.apiEndpoint}/analytics/${id}`, body);
}

export function deleteDashboard(id) {
    return http.delete(`${config.apiEndpoint}/analytics/${id}`);
}
