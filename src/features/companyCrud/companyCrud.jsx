import React, { useState } from "react";
import _ from "lodash";
import { CompanyTable } from "./company__table";
import { CompanySearch } from "./company__search";
import { CompanyForm } from "./company__form";
import { useModal } from "core/hooks/modalHook";
import { toast } from "react-toastify";
import { deleteCompany } from "./companyCrudService";
export const CompanyCrud = () => {
  const [companies, setCompanies] = useState([]);
  const [action, setAction] = useState("");
  const [selectedInput, setSelectedInput] = useState();
  const [selectedId, setSelectedId] = useState(0);
  const handleSearchResults = (output) => {
    // console.log(output);
    setCompanies(output);
  };

  // Search Options

  // Add Action
  const handleAddNew = () => {
    setAction("ADD");
    setSelectedId(0);
    setSelectedInput({});
    openFormModal();
  };

  // Edit Action
  const handleEdit = (data) => {
    setAction("EDIT");
    setSelectedId(data["id"]);
    setSelectedInput(data);
    openFormModal();
  };

  // Delete Action
  const handleDelete = async (data) => {
    try {
      setAction("DELETE");
      setSelectedId(data["id"]);
      setSelectedInput(data);
      const { data: company } = await deleteCompany(data["id"]);
      toast.success("company deleted successfully");
      const newList = [...companies];
      _.remove(newList, function (n) {
        console.log("Checking " + data["id"] + " " + n["id"]);
        return n["id"] == data["id"];
      });
      // console.log(newList);
      setCompanies(newList);
    } catch (error) {
      console.log(error);
      toast.error("Error deleting company");
    }
  };

  // Update on success
  const handleAddUpdateSuccess = (updatedData) => {
    if (action === "EDIT") {
      const index = _.findIndex(companies, { id: selectedId });
      const updatedDataList = [...companies];
      updatedDataList[index] = updatedData;
      setCompanies(updatedDataList);
    } else if (action === "ADD") {
      const newList = [...companies];
      newList.push(updatedData);
      setCompanies(newList);
    }
  };

  const [formModal, openFormModal, closeFormModal] = useModal(false);

  return (
    <React.Fragment>
      <CompanySearch
        onSearchResults={handleSearchResults}
        onAddNew={handleAddNew}
      />
      <CompanyTable
        data={companies}
        onEdit={handleEdit}
        onDelete={handleDelete}
      />
      <CompanyForm
        open={formModal}
        onClose={closeFormModal}
        selectedData={selectedInput}
        onUpdateSuccess={handleAddUpdateSuccess}
      />
    </React.Fragment>
  );
};
