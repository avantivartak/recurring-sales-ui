import { FormCard } from "core/cards/formCard";
import { EditTextField, SelectContainer, TextAreaField } from "core/components/inputWithAccessControl";
import { Modal } from "core/components/modal";
import { useLoading } from "core/hooks/loadingHook";
import { useSubmitForm } from "core/hooks/submitFormHook";
import Joi from "joi";
import React, { useEffect } from "react";
import { toast } from "react-toastify";
import _ from "lodash";
import { addCompany, editCompany } from "./companyCrudService";
import { addDefaultOption, formatArrayToOptions } from "core/utility/util";
import { useMasterListState } from "features/masterList/masterListProvider";

export const CompanyForm = ({
  open,
  onClose,
  selectedData,
  onUpdateSuccess,
}) => {
  const { loaderContainer, startLoading, stopLoading } = useLoading();

//vendor Options

const { vendorsList } = useMasterListState();

const sortedVendorList = _.sortBy(vendorsList, [ "name"]);
const vendorOptions = _.map(sortedVendorList, (site) => {
  const {  name } = site;
  return { label: `${name}`};
});
const vendorsOptions = addDefaultOption(vendorOptions);

  const schema = Joi.object({
    id: Joi.number().label("Id").positive().integer().allow(0),
    name: Joi.string().trim().label("Company Name").required().min(3).max(100),
    plotNo: Joi.string().trim().label("Plot No.").required(),
    vendor: Joi.string().trim().label("Vendor").required(),
    // vendorId: Joi.number().label("Vendor").positive().required(),
    address: Joi.string().trim().label("Address").required(),
    // logo: Joi.any().label("Logo File"),
  });
  const accessToEdit = true;
  const submitCallback = async (event) => {
    // console.log(inputs);
    const { id, name,plotNo,vendor,address } = inputs;
    try {
      startLoading();

      const body = {
        id,
        name,
        plotNo,
        vendor,
        address
      };
      // console.log(name);
      // console.log(body);
      // let fd = new FormData();
      // fd.append("logo2", inputs["logo"], "logo2.png");
      // fd.append("name", inputs["name"]);
      // fd.append("category", inputs["category"]);
      // fd.append("contactPerson", inputs["contactPerson"]);
      // fd.append("contactNo", inputs["contactNo"]);
      // fd.append("address", inputs["address"]);

      // console.log(fd);
      // for (var pair of fd.entries()) {
      //   console.log(pair[0] + ", " + pair[1]);
      // }
      if (
        id === null ||
        id === undefined ||
        id === "" ||
        id === "0" ||
        id === 0
      ) {
        const { data: company } = await addCompany(body);
        onUpdateSuccess(company);
        toast.success("Company Added Successfully");
      } else {
        const { data: company } = await editCompany(id, body);
        onUpdateSuccess(company);
        toast.success("Company Edited Successfully");
      }
      //   console.log(companies);
      stopLoading();
    } catch (error) {
      console.log(error);
      toast.error("Error adding / editing Company details");
      stopLoading();
    }
    onClose();
  };
  let defaultInputs = { id: 0 };
  useEffect(() => {
    if (!_.isEmpty(selectedData)) {
      handleInputChange("id", selectedData["id"]);
      handleInputChange("name", selectedData["name"]);
      handleInputChange("plotNo", selectedData["plotNo"]);
      handleInputChange("vendor", selectedData["vendor"]);
      // handleInputChange("vendorId", selectedData["vendorId"]);
      handleInputChange("address", selectedData["address"]);
    } else {
      resetInput();
      handleInputChange("id", 0);
    }
  }, [selectedData]);
  const {
    inputs,
    errors,
    handleInputChange,
    handleSubmit,
    resetInput,
    additionalValidation,
  } = useSubmitForm(schema, submitCallback, defaultInputs);

  // Category options
  
  const FormHtml = (
    <FormCard
      formName={"Add/Update Company"}
      onSubmit={handleSubmit}
      onFormReset={resetInput}
      submitAccess={accessToEdit}
    >
      <div className="columns is-multiline">
        <div className="column is-half">
          <EditTextField
            identifier="id"
            labelName="Id"
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
          />
        </div>

        <div className="column is-half">
          <EditTextField
            identifier="name"
            labelName="Company Name"
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
          />
        </div>
        {/* /.column */}
        <div className="column is-half">
          <EditTextField
            identifier="plotNo"
            labelName="Plot No."
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
          />
        </div>
        <div className="column is-half">
          <SelectContainer
            identifier="vendor"
            labelName="Vendor"
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
            options={vendorsOptions}
          />
        </div>
        {/* /.column */}
        {/* /.column */}
        {/* /.column */}
        <div className="column is-full">
          <TextAreaField
            identifier="address"
            labelName="Address"
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
            rows={2}
          />
        </div>
        {/* <div className="column is-one-third">
          <FileUpload
            identifier="logo"
            labelName="Logo"
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
          />
        </div> */}
        {/* /.column */}
        {/* /.column */}
      </div>
      {/* /.columns */}
    </FormCard>
  );

  return (
    <Modal open={open} onClose={onClose} width="full-width">
      <div>{FormHtml}</div>
      {loaderContainer}
    </Modal>
  );
};
