import config from "config.json";
import http from "core/service/httpService";

export function searchCompany(body) {
  return http.get(`${config.apiEndpoint}/companies/search`, {
    params: body,
  });
}

export function findAll(body) {
  return http.get(`${config.apiEndpoint}/companies`, {
    params: body,
  });
}

// export function addCompany(body) {
//   return http.filePost(`${config.apiEndpoint}/companies`, body);
// }

export function addCompany(body) {
  return http.post(`${config.apiEndpoint}/companies`, body);
}

export function editCompany(id, body) {
  return http.put(`${config.apiEndpoint}/companies/${id}`, body);
}

export function deleteCompany(id) {
  return http.delete(`${config.apiEndpoint}/companies/${id}`);
}
