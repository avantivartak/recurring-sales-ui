import React, { useState } from "react";
import _ from "lodash";
import { LicenceTable } from "./licence__table";
import { LicenceSearch } from "./licence__search";
import { LicenceForm } from "./licence__form";
import { useModal } from "core/hooks/modalHook";
import { toast } from "react-toastify";
import { deleteLicence } from "./licenceCrudService";
import { LicenceAlertTo } from "./licence__alert";
export const LicenceCrud = () => {
  const [licences, setLicences] = useState([]);
  const [action, setAction] = useState("");
  const [selectedInput, setSelectedInput] = useState();
  const [selectedId, setSelectedId] = useState(0);
  const handleSearchResults = (output) => {
    // console.log(output);
    setLicences(output);
  };

  // Search Options

  // Add Action
  const handleAddNew = () => {
    // setAction("ADD");
    // setSelectedId(0);
    // setSelectedInput({});
    // openFormModal();
    toast.warn("This feature is not implemented");
  };

  // Edit Action
  const handleEdit = (data) => {
    setAction("EDIT");
    setSelectedId(data["nodeId"]);
    setSelectedInput(data);
    openFormModal();
  };

  // Delete Action
  const handleDelete = async (data) => {
    try {
      setAction("DELETE");
      setSelectedId(data["nodeId"]);
      setSelectedInput(data);
      const { data: licence } = await deleteLicence(data["nodeId"]);
      toast.success("Licence deleted successfully");
      const newList = [...licences];
      _.remove(newList, function (n) {
        // console.log("Checking " + data["id"] + " " + n["id"]);
        return n["nodeId"] === data["nodeId"];
      });
      // console.log(newList);
      setLicences(newList);
    } catch (error) {
      console.log(error);
      toast.error("Error deleting licence");
    }
  };

  // AlertTo Action
  const [formAlertToModal, openFormAlertToModal, closeFormAlertToModal] =
    useModal(false);
  const handleAlertToEdit = (data) => {
    setAction("ALERT_TO_EDIT");
    setSelectedId(data["nodeId"]);
    setSelectedInput(data);
    openFormAlertToModal();
  };

  // Update on success
  const handleAddUpdateSuccess = (updatedData) => {
    if (action === "EDIT") {
      const index = _.findIndex(licences, { nodeId: selectedId });
      const updatedDataList = [...licences];
      updatedDataList[index] = updatedData;
      setLicences(updatedDataList);
    } else if (action === "ADD") {
      const newList = [...licences];
      newList.push(updatedData);
      setLicences(newList);
    }
  };

  const [formModal, openFormModal, closeFormModal] = useModal(false);

  return (
    <React.Fragment>
      <LicenceSearch
        onSearchResults={handleSearchResults}
        onAddNew={handleAddNew}
      />
      <LicenceTable
        data={licences}
        onEdit={handleEdit}
        onDelete={handleDelete}
        onAlertToEdit={handleAlertToEdit}
      />
      <LicenceForm
        open={formModal}
        onClose={closeFormModal}
        selectedData={selectedInput}
        onUpdateSuccess={handleAddUpdateSuccess}
      />
      <LicenceAlertTo
        open={formAlertToModal}
        onClose={closeFormAlertToModal}
        selectedData={selectedInput}
        onUpdateSuccess={handleAddUpdateSuccess}
      />
    </React.Fragment>
  );
};
