import React, { useEffect, useState } from "react";
import { useSubmitForm } from "core/hooks/submitFormHook";
import Joi from "joi";
import { FormCard } from "core/cards/formCard";
import { EditTextField } from "core/components/inputWithAccessControl";
import _ from "lodash";
import { searchLicenceByLicenceType } from "./licenceCrudService";
import { addDefaultOption } from "core/utility/util";
import { formatArrayToOptions } from "core/utility/util";
import { SelectContainer } from "core/components/inputWithAccessControl";
import { useMasterListState } from "features/masterList/masterListProvider";
import { toast } from "react-toastify";
import { useLoading } from "core/hooks/loadingHook";
import { saveLicenceAlertTos } from "./licenceCrudService";

export const AlertToArrayForm = ({ selectedData, onClose }) => {
  const { alertTo, nodeId: id } = selectedData;

  // console.log(selectedData);
  const accessToEdit = true;

  const saveAlertToData = async (result) => {
    try {
      startLoading();
      const body = result;
      // console.log(body);
      const { data: parameter } = await saveLicenceAlertTos(id, body);
      toast.success("AlertTo Added Successfully");

      stopLoading();
    } catch (error) {
      console.log(error);
      toast.error("Error adding / editing Licence details");
      stopLoading();
    }
    onClose();
  };

  const handleSubmit = async (event) => {
    if (event) {
      event.preventDefault();
    }
    // console.log(alertToList);
    const result = schema.validate(alertToList, { abortEarly: false });
    if (result.error) {
      // console.log(result);
      toast.error("Validation Error");
    } else {
      // console.log(result);
      saveAlertToData(result.value);
    }
  };

  const objectSchema = Joi.object({
    informTo: Joi.string().trim().label("Inform At").required(),
    modeOfInformation: Joi.string().trim().label("Mode of Alert").required(),
  });
  const schema = Joi.array().items(objectSchema).min(1).unique().required();
  const [alertToList, setAlertToList] = useState([]);

  // const [updateAlertToList, setUpdateAlertToList]= useState([]);

  useEffect(() => {
    if (alertTo) {
      setAlertToList(alertTo);
    }
  }, [alertTo]);
  const handleAddRow = () => {
    // console.log("Add row clicked");
    const newList = [...alertToList];
    newList.push({});
    setAlertToList(newList);
  };
  const handleEditRow = (arrayIndex, name, value) => {
    // console.log(`Row edited ${arrayIndex} ${name} ${value}`);
    const updatedData = alertToList[arrayIndex];
    updatedData[name] = value;
    const updatedDataList = [...alertToList];
    updatedDataList[arrayIndex] = updatedData;
    setAlertToList(updatedDataList);
  };
  const handleDeleteRow = (arrayIndex) => {
    // console.log(`Delete row clicked ${arrayIndex}`);
    const newList = [...alertToList];
    newList.splice(arrayIndex, 1);
    // console.log(newList);
    setAlertToList(newList);
  };

  const options = {
    buttons: (
      <button
        type="button"
        className="button is-info is-small mx-2"
        onClick={handleAddRow}
      >
        Add Alert Row
      </button>
    ),
  };
  let alertToRowList = [];
  _.forEach(alertToList, (alertTo, index) => {
    // console.log(p);
    alertToRowList.push(
      <AlertToRow
        key={index}
        arrayIndex={index}
        schema={objectSchema}
        alertTo={alertTo}
        onRowEdit={handleEditRow}
        onRowDelete={handleDeleteRow}
      />
    );
  });

  const { loaderContainer, startLoading, stopLoading } = useLoading();
  return (
    <React.Fragment>
      <FormCard
        formName={"Add/Update Alerto"}
        onSubmit={handleSubmit}
        submitAccess={accessToEdit}
        options={options}
      >
        {alertToRowList}
      </FormCard>
      {loaderContainer}
    </React.Fragment>
  );
};

const AlertToRow = ({
  arrayIndex,
  schema,
  alertTo,
  onRowEdit,
  onRowDelete,
}) => {
  // console.log(alertTo);
  //Mode of information Options
  const modeOfInformation = ["SMS", "EMAIL"];
  const modeOfInformationOptions = addDefaultOption(
    formatArrayToOptions(modeOfInformation)
  );

  useEffect(() => {
    if (alertTo) {
      const { modeOfInformation, informTo } = alertTo;
      handleInputChange("modeOfInformation", modeOfInformation);
      handleInputChange("informTo", informTo);
    }
  }, [alertTo]);

  const submitCallback = () => {};
  let defaultInputs = {};
  useEffect(() => {});

  const { inputs, errors, handleInputChange } = useSubmitForm(
    schema,
    submitCallback,
    defaultInputs
  );

  // Custom input change
  const handleCustomInputChange = (name, value) => {
    onRowEdit(arrayIndex, name, value);
    handleInputChange(name, value);
  };

  return (
    <div className="box">
      <div className="columns is-multiline">
        <div className="column is-half">
          <SelectContainer
            identifier="modeOfInformation"
            labelName="Mode of Alert"
            handleInputChange={handleCustomInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
            options={modeOfInformationOptions}
          />
        </div>
        {/* /.column */}
        <div className="column is-half">
          <EditTextField
            identifier="informTo"
            labelName="Inform At"
            handleInputChange={handleCustomInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
          />
        </div>
        {/* /.column */}

        <div className="column is-full">
          <button
            type="button"
            className="m-2 button is-small is-danger"
            onClick={() => onRowDelete(arrayIndex)}
          >
            Delete Row
          </button>
        </div>
        {/* /.column */}
      </div>
      {/* /.columns */}
    </div>
  );
};
