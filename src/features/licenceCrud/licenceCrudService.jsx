import config from "config.json";
import http from "core/service/httpService";

export function searchLicence(body) {
  return http.get(`${config.apiEndpoint}/licenses/search/${body.name}`);
}

export function findAll(body) {
  return http.get(`${config.apiEndpoint}/licenses`, {
    params: body,
  });
}

// export function addLicence(body) {
//   return http.filePost(`${config.apiEndpoint}/licenses`, body);
// }

export function addLicence(body) {
  return http.post(`${config.apiEndpoint}/licenses`, body);
}

export function editLicence(id, body) {
  return http.put(`${config.apiEndpoint}/licenses/${id}`, body);
}

export function deleteLicence(id) {
  return http.delete(`${config.apiEndpoint}/licenses/${id}`);
}

export function saveLicenceAlertTos(id, body) {
  return http.put(`${config.apiEndpoint}/licenses/alertTo/${id}`, body);
}
