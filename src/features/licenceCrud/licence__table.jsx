import { DeleteIcon } from "core/components/svgIcon";
// import { ParameterIcon } from "core/components/svgIcon";
import { EditIcon } from "core/components/svgIcon";
import { ParameterIcon } from "core/components/svgIcon";
import { AlertIcon } from "core/components/svgIcon";
import { Table } from "core/components/table";
import { useConfirmBox } from "core/hooks/confirmHook";
import { useModal } from "core/hooks/modalHook";
import _ from "lodash";
import React, { useState } from "react";

export const LicenceTable = ({ data, onEdit, onDelete, onAlertToEdit }) => {
  // Edit action

  const handleEditAction = (value) => {
    const licenceData = _.pick(value, [
      "nodeId",
      "siteName",
      "companyName",
      "vendor",
      "nodeName",
      "nodeType",
      "status",
      "renewDate",
      "expDate",
      "amount",
      "connectivity",
    ]);
    onEdit(licenceData);
  };

  const handleAlertEditAction = (value) => {
    console.log(value);
    const licenceData = _.pick(value, ["nodeId", "alertTo"]);
    onAlertToEdit(licenceData);
  };

  // Delete Action

  const options = { onSuccess: onDelete };
  const { confirmContainer, openConfirmBox, setSelectedInput } =
    useConfirmBox(options);

  const handleDeleteAction = (value) => {
    setSelectedInput(value);
    openConfirmBox();
  };

  const defaultColDef = {
    sortable: true,
    resizable: true,
    filter: true,
    floatingFilter: true,
    flex: 1,
    minWidth: 150,
  };

  const columnDefs = [
    {
      headerName: "Licence",
      children: [
        {
          headerName: "Site Name",
          field: "siteName",
          maxWidth: 250,
        },
        {
          headerName: "Company Name",
          field: "companyName",
          maxWidth: 250,
        },{
          headerName: "Vendor Name",
          field: "vendor",
          maxWidth: 250,
        },
        {
          headerName: "Device Type",
          field: "nodeType",
          maxWidth: 200,
        },
        {
          headerName: "Device Name",
          field: "nodeName",
          maxWidth: 250,
        },
        {
          headerName: "Renew Date",
          field: "renewDate",
          minWidth: 180,
        },
        {
          headerName: "Expiry Date",
          field: "expDate",
          minWidth: 180,
        },
        {
          headerName: "Status",
          field: "status",
          minWidth: 100,
        },{
          headerName: "Amount",
          field: "amount",
          minWidth: 150,
        },
        {
          headerName: "Connectivity",
          field: "connectivity",
          minWidth: 200,
        },
        {
          headerName: "Details",
          valueGetter: function (params) {
            return params.data;
          },
          cellRenderer: "actionBtnCellRenderer",
          sortable: false,
          filter: false,
          minWidth: 300,
        },
      ],
    },
  ];

  const ActionButtonRenderer = ({ value }) => {
    return (
      <React.Fragment>
        <span
          className="mx-2 is-clickable"
          title="Edit"
          onClick={() => handleEditAction(value)}
        >
          <EditIcon />
        </span>
        <span
          title="Delete"
          className="mx-2 is-clickable"
          onClick={() => handleDeleteAction(value)}
        >
          <DeleteIcon />
        </span>
        <span
          title="Alert"
          className="mx-2 is-clickable"
          onClick={() => handleAlertEditAction(value)}
        >
          <AlertIcon />
        </span>
      </React.Fragment>
    );
  };

  const frameworkComponents = {
    actionBtnCellRenderer: ActionButtonRenderer,
  };

  return (
    <React.Fragment>
      <Table
        rowData={data}
        columnDefs={columnDefs}
        defaultColDef={defaultColDef}
        paginationAutoPageSize={true}
        pagination={true}
        frameworkComponents={frameworkComponents}
      />
      {confirmContainer}
    </React.Fragment>
  );
};
