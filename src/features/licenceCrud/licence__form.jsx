import { FormCard } from "core/cards/formCard";
import { TextAreaField } from "core/components/inputWithAccessControl";
import { SelectContainer ,MultiSelectContainer} from "core/components/inputWithAccessControl";
import { EditTextField } from "core/components/inputWithAccessControl";
import { DateField } from "core/components/inputWithAccessControl";
import { Modal } from "core/components/modal";
import { useLoading } from "core/hooks/loadingHook";
import { useSubmitForm } from "core/hooks/submitFormHook";
import { formatArrayToOptions } from "core/utility/util";
import { addDefaultOption } from "core/utility/util";
import Joi from "joi";
import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import _ from "lodash";
import { addLicence } from "./licenceCrudService";
import { FileUpload } from "core/components/inputWithAccessControl";
import { editLicence } from "./licenceCrudService";
import { useMasterListState } from "features/masterList/masterListProvider";
import { SearchableSelectContainer } from "core/components/inputWithAccessControl";
import { CreatableSelectContainer } from "core/components/inputWithAccessControl";



export const LicenceForm = ({
  open,
  onClose,
  selectedData,
  onUpdateSuccess,
}) => {
  const { loaderContainer, startLoading, stopLoading } = useLoading();

  const schema = Joi.object({
    nodeId: Joi.number().label("Node Id").positive().allow(0),
    siteName:Joi.string().trim().label("Site Name"),
    companyName:Joi.string().trim().label("Company Name"),
    nodeType:Joi.string().trim().label("node Type"),
    nodeName:Joi.string().trim().label("node Name"),
    vendor:Joi.string().trim().label("Vendor Name").required(),
    status: Joi.string().trim().label("Status").required(),
    renewDate: Joi.date().label("Renew Date").required(),
    expDate: Joi.date().label("Expiry Date").required(),
    amount:Joi.string().trim().label("Amount"),
    connectivity: Joi.array().items(Joi.string()).default([]),
    alertTo: Joi.array().items(Joi.object()),
   

  });
  const accessToEdit = true;
  const submitCallback = async (event) => {
    // console.log(inputs);
    
    const { nodeId,siteName,companyName,nodeType,nodeName,vendor,status, renewDate, expDate, amount, connectivity, alertTo} = inputs;
    try {
      startLoading();

      const body = {
        nodeId,
        siteName,companyName,nodeType,nodeName,
        vendor,
        status,
        renewDate,
        expDate,
        amount,
        connectivity,
        alertTo,

      };
      // console.log(body);
      if (
        nodeId === null ||
        nodeId === undefined ||
        nodeId === "" ||
        nodeId === "0" ||
        nodeId === 0
      ) {
        const { data: licence } = await addLicence(body);
        onUpdateSuccess(licence);
        toast.success("Licence Added Successfully");
      } else {
        const { data: licence } = await editLicence(nodeId, body);
        onUpdateSuccess(licence);
        console.log(licence);
        toast.success("Licence Edited Successfully");
      }
      //   console.log(companies);
      stopLoading();
    } catch (error) {
      console.log(error);
      toast.error("Error adding / editing licence details");
      stopLoading();
    }
    onClose();
  };
  let defaultInputs = { nodeId: 0 };
  useEffect(() => {
    if (selectedData) {
      console.log(selectedData);
      handleInputChange("nodeId", selectedData["nodeId"]);
      handleInputChange("siteName", selectedData["siteName"]);
      handleInputChange("companyName", selectedData["companyName"]);
      handleInputChange("nodeType", selectedData["nodeType"]);
      handleInputChange("nodeName", selectedData["nodeName"]);
      handleInputChange("vendor", selectedData["vendor"]);
      handleInputChange("status", selectedData["status"]);
      handleInputChange("renewDate", selectedData["renewDate"]);
      handleInputChange("expDate", selectedData["expDate"]);
      handleInputChange("alertTo", selectedData["alertTo"]);
      handleInputChange("amount", selectedData["amount"]);
      handleInputChange("connectivity", selectedData["connectivity"]);

    } else {
      resetInput();
      handleInputChange("nodeId", 0);
    }
  }, [selectedData]);
  const {
    inputs,
    errors,
    handleInputChange,
    handleSubmit,
    resetInput,
    additionalValidation,
  } = useSubmitForm(schema, submitCallback, defaultInputs);

  const licenceStatus = ["ON", "OFF"];
  const licenceStatusOption = addDefaultOption(
    formatArrayToOptions(licenceStatus)
  );
  
  const licenceVendor=["VAA","SVAN","LAB SYSTEMS"];
  const licenceVendorOption= addDefaultOption(  formatArrayToOptions(licenceVendor));
  // console.log(licenceVendorOption);

  const connectivityType=["CPCB","SPCB","CETP"];
  const connectivityTypeOption= formatArrayToOptions(connectivityType);

  

  const FormHtml = (
    <FormCard
      formName={"Add/Update Licence"}
      onSubmit={handleSubmit}
      onFormReset={resetInput}
      submitAccess={accessToEdit}
    >
      <div className="columns is-multiline">
        <div className="column is-half ">
          <EditTextField
            identifier="nodeId"
            labelName="Node Id"
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
          />
        </div>
        
        <div className="column is-half is-field">
        <CreatableSelectContainer
            identifier="vendor"
            labelName="Vendor Name"
            handleInputChange={handleInputChange}
            placeholder="vendor"
            inputs={inputs}
            errors={errors}
            editAccess={true}
            options={licenceVendorOption}
            
          />
        </div>
        <div className="column is-half">
          <SelectContainer
            identifier="status"
            labelName="Licence Status"
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
            options={licenceStatusOption}
          />
        </div>

        <div className="column is-half">
          <DateField
            identifier="renewDate"
            labelName="Renew Date (yyyy-mm-dd)"
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
            max={new Date(2030, 12, 31)}
            format={"YYYY-MM-DD"}
          />
        </div>
        {/* /.column */}
        <div className="column is-half">
          <DateField
            identifier="expDate"
            labelName="Expiry Date (yyyy-mm-dd)"
            handleInputChange={handleInputChange}
            max={new Date(2030, 12, 31)}
            inputs={inputs}
            errors={errors}
            editAccess={true}
          />
        </div>
        <div className="column is-half ">
          <EditTextField
            identifier="amount"
            labelName="Amount"
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}

          />
        </div>
        <div className="column is-half">
        <MultiSelectContainer
            identifier="connectivity"
            labelName="Connectivity"
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
            options={connectivityTypeOption}
            // required={false}
          />
        </div>
        {/* /.column */}

        {/* <div className="column is-one-third">
          <EditTextField
            identifier="slaveId"
            labelName="Slave Id"
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
          />
        </div> */}
        {/* <div className="column is-one-third">
          <SearchableSelectContainer
            identifier="siteId"
            labelName="Site Name"
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
            options={siteNameOptions}
          />
        </div> */}

        {/* <div className="column is-one-third">
          <EditTextField
            identifier="encryptionKey"
            labelName="Encryption Key"
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
          />
        </div> */}

        {/* /.column */}
        {/* <div className="column is-one-third">
          <FileUpload
            identifier="logo"
            labelName="Logo"
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
          />
        </div> */}
        {/* /.column */}

        {/* /.column */}
        {/* <div className="column is-one-third">
          <EditTextField
            identifier="make"
            labelName="Make"
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
          />
        </div> */}
        {/* /.column */}
        {/* <div className="column is-one-third">
          <EditTextField
            identifier="model"
            labelName="Model"
            handleInputChange={handleInputChange}
            inputs={inputs}
            errors={errors}
            editAccess={true}
          />
        </div> */}
        {/* /.column */}
      </div>
      {/* /.columns */}
    </FormCard>
  );

  return (
    <Modal open={open} onClose={onClose} width="full-width">
      <div>{FormHtml}</div>
      {loaderContainer}
    </Modal>
  );
};
