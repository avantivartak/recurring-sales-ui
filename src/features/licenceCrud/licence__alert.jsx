import { Modal } from "core/components/modal";
import { TabContainer } from "core/components/tab";
import { Panel } from "core/components/tab";
import React from "react";
import { AlertToArrayForm } from "./licence__alert__form";

export const LicenceAlertTo = ({ open, onClose, selectedData }) => {
  // console.log(selectedData);
  if (!selectedData) {
    return "";
  }

  // const CalAlertToHtml = (
  //   <div className="m-2 p-2"> Calibration AlertTos</div>
  // );
  // const FormHtml = <div className="m-2 p-2">Diagnostic AlertTos</div>;
  return (
    <Modal open={open} onClose={onClose} width="full-width">
      <div className="card">
        <AlertToArrayForm
          selectedData={selectedData}
          onClose={onClose}
        />
      </div>
    </Modal>
  );
};
