import React from "react";

export const ConfirmBox = (options) => {
  const { onSuccess, onFailure } = options;
  const [confirmBox, openConfirmBox, closeConfirmBox] = useModal(false);

  const loaderContainer = (
    <React.Fragment>
      <Modal open={confirmBox} onClose={onClose}>
        <div className="card">
          <header className="card-header">
            <p className="card-header-title is-size-6">Confirm Box</p>
          </header>
          {/* /.card-header */}
          <div className="card-content">
            <div className="custom-ui">
              <h1>Are you sure?</h1>
              <p>You want to delete this file?</p>
              <button onClick={onClose}>No</button>
              <button
                onClick={() => {
                  this.handleClickDelete();
                  onClose();
                }}
              >
                Yes, Delete it!
              </button>
            </div>
          </div>
        </div>
        {/* /.card */}
      </Modal>
    </React.Fragment>
  );

  return {
    confirmContainer,
  };
};
