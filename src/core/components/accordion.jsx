import React from "react";

export const Accordion = (props) => {
  const { children } = props;
  return <div className="accordion-container">{children}</div>;
};

export const AccordionItem = (props) => {
  const { id, label, children } = props;
  return (
    <div className="accordion">
      <input type="checkbox" className="accordion__input" id={id} />
      <label className="accordion__label" htmlFor={id}>
        <h5 className="title is-5">{label}</h5>
      </label>
      <div className="accordion__content">{children}</div>
    </div>
  );
};
