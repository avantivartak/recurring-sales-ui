import { useLoading } from 'core/hooks/loadingHook';
import { downloadReport } from 'core/utility/util';
import { generateReport } from 'features/nodeCrud/nodeCrudService';
import React from 'react'
import { toast } from 'react-toastify';
import { DownloadIcon } from './svgIcon'

export const DashCard = (props) => {
    const { loaderContainer, startLoading, stopLoading } = useLoading();

    const handleDownload = async () => {
        startLoading();

        let templateName = "TOTAL_OFFLINE_NODES";

        if (props.connection === "Nodes Offline Since yesterday") {
            templateName = "OFFLINE_NODES_YESTERDAY";
        }
        const body = {
            templateName: templateName,
            reportName: "Node Status Report",
            exportFormat: "pdf",

        };
        // console.log(body);
        try {
            const response = await generateReport(body);
            let fileName = response.headers["x-blob-file"];
            downloadReport(fileName, response);
            toast.success("Report generated successfully");
            stopLoading();
        } catch (error) {
            console.log(error);
            toast.error("Error while generating report");
            stopLoading();
        }
    };



    return (

        <div className="column is-3 is-3-fullhd is-3-desktop is-6-tablet is-12-mobile is-one-quarter">
            <div className="serviceBox">
                <div className="service-content">
                    <div className="service-icon">
                        <span />
                        <img className="img-size" src={props.img} />
                    </div>
                    <div style={{ textAlign: 'end', flex: '1', marginTop: '7px' }}>
                        <h3 className="title" style={props.style}>{props.connection}</h3>
                        <p className="description">{props.value}</p>
                        {props.isButton &&
                            <div>
                                <button class="button btn-card-dash is-small" onClick={handleDownload}>
                                    <span class="icon is-small">
                                        <DownloadIcon />    </span><span>Reports</span>
                                </button>
                                {/* <button className="button btn-card-dash is-small" style={props.style}>Reports</button> */}
                            </div>}

                    </div>
                </div>
            </div>
        </div>)
}
