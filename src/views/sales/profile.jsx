import React from "react";

import { UpdatePassword } from "features/user/updatePassword";
import { BreadcrumbContainer, BreadcrumbItem } from "core/components/breadcrumb";
import { PageHeader } from "core/components/pageHeader";

export const UserProfile = (props) => {


  return (
    <React.Fragment>
         <div className="content-container__header">
        <PageHeader
          header={"Update Password"}
          description={"Interface for Update Password"}
        />
        <BreadcrumbContainer>
          <BreadcrumbItem label={"Home"} />
          <BreadcrumbItem label={"Update Password"} active={true} />
        </BreadcrumbContainer>
      </div>
    <div className="container">
      
      {/* /.card */}
      <UpdatePassword {...props} />
    </div>
    </React.Fragment>

  );
};
