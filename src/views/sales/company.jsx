import React from "react";
import { BreadcrumbItem } from "core/components/breadcrumb";
import { BreadcrumbContainer } from "core/components/breadcrumb";
import { PageHeader } from "core/components/pageHeader";
import { CompanyCrud } from "features/companyCrud/companyCrud";


export const Company = () => {
  // console.log(nodes);
  // console.log(processTags);
  return (
    <React.Fragment>
      <div className="content-container__header">
        <PageHeader
          header={"Company"}
          description={"details about company"}
        />
        <BreadcrumbContainer>
          <BreadcrumbItem label={"Home"} />
          <BreadcrumbItem label={"Company"} active={true} />
        </BreadcrumbContainer>
      </div>
      {/* /.content__header */}

      <div className="content-container__body">
        <div className="dashboard">
          <div className="dashboard__tab-content">
            <CompanyCrud/>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
