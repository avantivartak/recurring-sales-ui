import React from "react";
import { BreadcrumbItem } from "core/components/breadcrumb";
import { BreadcrumbContainer } from "core/components/breadcrumb";
import { PageHeader } from "core/components/pageHeader";
import { CompanyCrud } from "features/companyCrud/companyCrud";
import { BillingCrud } from "features/billingCrud/billingCrud";


export const Billing = () => {
  // console.log(nodes);
  // console.log(processTags);
  return (
    <React.Fragment>
      <div className="content-container__header">
        <PageHeader
          header={"Billling"}
          description={"details about Billling"}
        />
        <BreadcrumbContainer>
          <BreadcrumbItem label={"Home"}/>
          <BreadcrumbItem label={"Billling"} active={true} />
        </BreadcrumbContainer>
      </div>
      {/* /.content__header */}

      <div className="content-container__body">
        <div className="dashboard">
          <div className="dashboard__tab-content">
            <BillingCrud/>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
