import React, { Component } from "react";
import { NavLink } from "core/components/navLink";
import { BillingIcon, CompanyIcon, ServiceIcon, UserIcon } from "core/components/svgIcon";
// import { NavLinkMultilevel } from "core/components/navLink";

export const SalesSidebar = () => {
  return (
    <aside className="menu">
      <p className="menu-label">General</p>
      <ul className="menu-list">
        <NavLink link="/sales/company"><div className="is-flex"><CompanyIcon/><span className="pl-2"> Company</span></div></NavLink>
        <NavLink link="/sales/service"><div className="is-flex"><ServiceIcon/><span className="pl-2"> Service</span></div></NavLink>
        <NavLink link="/sales/billing"><div className="is-flex"><BillingIcon/><span className="pl-2"> Billing</span></div></NavLink>
        <NavLink link="/sales/profile"><div className="is-flex"><UserIcon/><span className="pl-2"> Profile</span></div></NavLink>
      </ul>
    </aside>
  );
};
