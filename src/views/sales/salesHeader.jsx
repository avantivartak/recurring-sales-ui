import React, { Component } from "react";
import { MenuIcon } from "core/components/svgIcon";
import { LogoIcon } from "core/components/svgIcon";
import salesImg from "../../images/sales-manager.png"
export const SalesHeader = (props) => {
  const { toggleSidebar, onLogout } = props;
  return (
    <React.Fragment>
      <div className="header__logo logo">
        <div className="logo__icon">
          <img src={salesImg}></img>
        </div>
        <h4 className="logo__text">RSA</h4>
      </div>
      <div className="header__burger" onClick={toggleSidebar}>
        <MenuIcon />
      </div>

      <div className="header__tray">
        <button className="button is-primary" onClick={onLogout}>
          Logout
        </button>
      </div>
    </React.Fragment>
  );
};
