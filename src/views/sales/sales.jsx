import useWindowDimensions from "core/hooks/windowDimensionsHook";
import { AuthenticationValidator } from "features/authentication/authenticationValidator";
import { logout } from "features/authentication/authService";
import { FeatureProvider } from "features/feature/featureProvider";
import { MasterlistProvider } from "features/masterList/masterListProvider";
import React, { useState } from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import { SalesHeader } from "./salesHeader";
import { SalesSidebar } from "./salesSidebar";
import { Company } from "./company";
import { ProductAction } from "./productActions";
import roles from "roles.json";
import { UserProfile } from "./profile";
import { Billing } from "./billing";

export const Sales = (props) => {
  const handleLogout = () => {
    logout();
    props.history.push("/");
  };

  const { width, height } = useWindowDimensions();
  const [sidebar, setSidebar] = useState(width > 1024);
  const toggleSidebar = () => {
    setSidebar(!sidebar);
  };
  let sidebarClass = "";
  if (sidebar) {
    sidebarClass = "sidebar";
  } else {
    sidebarClass = "sidebar-hidden";
  }
  const options = { requiredRole: roles.SALES };
  return (
    <FeatureProvider>
      <MasterlistProvider>
        <AuthenticationValidator options={options}>
          <React.Fragment>
            <div className="main">
              <header className="header">
                <SalesHeader
                  toggleSidebar={toggleSidebar}
                  onLogout={handleLogout}
                />
              </header>
              <div className="content-area">
                <div className={`${sidebarClass}`}>
                  <SalesSidebar />
                </div>
                <div className="content-container">
                  <Routes>
                    <Route
                      path="service"
                      element={<ProductAction/>}
                    />
                    <Route path="company" element={ <Company />} />
                    <Route
                      path="profile"
                      element={<UserProfile />}
                    />
                    <Route
                      path="billing"
                      element={<Billing />}
                    />
                    <Route path="/" element={<Navigate to="company" />} />
                  </Routes>
                </div>
              </div>
            </div>
          </React.Fragment>
        </AuthenticationValidator>
      </MasterlistProvider>
    </FeatureProvider>
  );
};
