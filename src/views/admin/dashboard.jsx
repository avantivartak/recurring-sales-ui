import React from "react";
import { BreadcrumbItem } from "core/components/breadcrumb";
import { BreadcrumbContainer } from "core/components/breadcrumb";
import { PageHeader } from "core/components/pageHeader";

export const Dashboard = () => {
  return (
    <React.Fragment>
      <div className="content-container__header">
        <PageHeader
          header={"Dashboard"}
          description={"Interface for handling Dashboard"}
        />
        <BreadcrumbContainer>
          <BreadcrumbItem label={"Home"} />
          <BreadcrumbItem label={"Dashboard"} active={true} />
        </BreadcrumbContainer>
      </div>
      {/* /.content__header */}
      <div className="content-container__body">
        <div className="dashboard">
          <div className="dashboard__tab-content">
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
