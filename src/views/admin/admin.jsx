import useWindowDimensions from "core/hooks/windowDimensionsHook";
import { AuthenticationValidator } from "features/authentication/authenticationValidator";
import { logout } from "features/authentication/authService";
import { FeatureProvider } from "features/feature/featureProvider";
import { MasterlistProvider } from "features/masterList/masterListProvider";
import React, { useState } from "react";
import { Navigate, Route, Routes, Switch, useNavigate } from "react-router-dom";
import { AdminHeader } from "./adminHeader";
import { AdminSidebar } from "./adminSidebar";
import { UserAction } from "./userActions";
import roles from "roles.json";
import { ProductAction } from "./productActions";
import { VendorAction } from "./vendorActions";
import { Dashboard } from "./dashboard";

export const Admin = () => {
  const navigate = useNavigate();
  const handleLogout = () => {
    logout();
    navigate("/");
  };

  const { width, height } = useWindowDimensions();
  const [sidebar, setSidebar] = useState(width > 1024);
  const toggleSidebar = () => {
    setSidebar(!sidebar);
  };
  let sidebarClass = "";
  if (sidebar) {
    sidebarClass = "sidebar";
  } else {
    sidebarClass = "sidebar-hidden";
  }
  const options = { requiredRole: roles.ADMIN };
  return (
    <FeatureProvider>
      <MasterlistProvider>
        <AuthenticationValidator options={options}>
          <React.Fragment>
            <div className="main">
              <header className="header">
                <AdminHeader
                  toggleSidebar={toggleSidebar}
                  onLogout={handleLogout}
                />
              </header>
              <div className="content-area">
                <div className={`${sidebarClass}`}>
                  <AdminSidebar />
                </div>
                <div className="content-container">
                  <Routes>
                    <Route
                      path="action/user"
                      element={<UserAction />}
                    />
                    <Route
                      path="action/product"
                      element={<ProductAction />}
                    />
                    <Route
                      path="action/vendor"
                      element={<VendorAction />}
                    />
                    <Route
                      path="action/dashboard"
                      element={<Dashboard />}
                    />
                    <Route path="/" element={<Navigate to="action/dashboard"/>}/>
                  </Routes>
                </div>
              </div>
            </div>
          </React.Fragment>
        </AuthenticationValidator>
      </MasterlistProvider>
    </FeatureProvider>
  );
};
