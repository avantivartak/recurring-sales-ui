import React from "react";
import { NavLink } from "core/components/navLink";
import { DashIcon, ProductIcon, UserIcon, VendorIcon } from "core/components/svgIcon";

export const AdminSidebar = () => {
  return (
    <aside className="menu">
      <p className="menu-label">General</p>
      <ul className="menu-list">
      <NavLink link="action/dashboard"><div className="is-flex"><DashIcon/><span className="pl-2"> Dashboard</span></div></NavLink>
      <NavLink link="action/user"><div className="is-flex"><UserIcon/><span className="pl-2"> User</span></div></NavLink>
      <NavLink link="action/product"><div className="is-flex"><ProductIcon/><span className="pl-2"> Product Type</span></div></NavLink>
      <NavLink link="action/vendor"><div className="is-flex"><VendorIcon/><span className="pl-2"> Vendor</span></div></NavLink>

      
      </ul>
    </aside>
  );
};
