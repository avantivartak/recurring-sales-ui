import { CapitalizedText } from "core/components/capitalizedText";
import { EditTextField } from "core/components/inputWithAccessControl";
import { useLoading } from "core/hooks/loadingHook";
import { useSubmitForm } from "core/hooks/submitFormHook";
import { saveRefreshToken } from "features/authentication/authService";
import { saveToken } from "features/authentication/authService";
// import { saveUser } from "features/authentication/authService";
import { login } from "features/authentication/authService";
import Joi from "joi";
import jwt_decode from "jwt-decode";
import _ from "lodash";
import React, { useState } from "react";
import { toast } from "react-toastify";
import roles from "roles.json";

export const Login = (props) => {
  const [type, setType] = useState('password');
  const handleToggle = () => {
    if (type === 'password') {
      setType('text')
    } else {
      setType('password')
    }
  }
  const schema = Joi.object({
    username: Joi.string().label("Login Id").trim().min(1).max(30).required(),
    password: Joi.string().label("Password").trim().min(1).max(30).required(),
  });
  const currentYear = new Date().getFullYear();

  const loginCallback = async () => {
    startLoading("Checking credentials....");
    try {
      const { username, password } = inputs;
      const response = await login(username, password);
      // const response = {
      //   data: {
      //     accessToken:
      //       "eyJhbGciOiJIUzI1NiJ9.eyJyb2xlIjoiW1wiQURNSU5cIl0iLCJJc3N1ZXIiOiJJc3N1ZXIiLCJleHAiOjE2MzE5MDk5NzEsImlhdCI6MTYzMTgyMzU3MX0.CPsw9mXsG4CBOSK4umUDAMpYQhvMiGDYBJ7DlphrFlU",
      //     refreshToken:
      //       "eyJhbGciOiJIUzI1NiJ9.eyJyb2xlIjoiW1wiQURNSU5cIl0iLCJJc3N1ZXIiOiJJc3N1ZXIiLCJleHAiOjE2MzE5MDk5NzEsImlhdCI6MTYzMTgyMzU3MX0.CPsw9mXsG4CBOSK4umUDAMpYQhvMiGDYBJ7DlphrFlU",
      //   },
      // };
      // console.log("response",response.data.accessToken);
      saveToken(response.data.accessToken);
      // console.log(response.data.accessToken);
      saveRefreshToken(response.data.refreshToken);
      const decodedToken = jwt_decode(response.data.accessToken);
      // console.log(decodedToken);
      const role = decodedToken.role;
      // console.log(role);
      toast.success("Logged In Successfully");
      stopLoading();
      // console.log(roles.COMPANY);
      // console.log(_.includes(role, roles.COMPANY));
      if (_.includes(role, roles.ADMIN)) {
        window.location = "/admin";
      }

      if (_.includes(role, roles.SALES)) {
        // console.log("Hello");
        window.location = "/sales";
      }
    } catch (error) {
      stopLoading();
      if (
        error.response &&
        error.response.status === 401 &&
        error.response.data &&
        error.response.data.message
      ) {
        toast.error(<CapitalizedText message={error.response.data.message} />);
      } else {
          console.log(error);
        toast.error(<CapitalizedText message={"Cannot connect to server"} />);
      }
    }
  };

  const { inputs, errors, handleInputChange, handleSubmit } = useSubmitForm(
    schema,
    loginCallback
  );

  // const [loading, setLoading] = useState(false);
  const { loaderContainer, startLoading, stopLoading } = useLoading();
  return (
    <div className="login login-bg">
      <div className="login__body ">
        <div className="login-card glass-bg" style={{borderRadius:"10px"}}>
          <form onSubmit={handleSubmit}>
            <h1 className="login-card__header title ">
             Recurring Sales App
            </h1>
            <div className="login-card__body">
              <EditTextField
                identifier="username"
                labelName="Login Id"
                handleInputChange={handleInputChange}
                inputs={inputs}
                errors={errors}
                editAccess={true}
              />
              <EditTextField
                identifier="password"
                labelName="Password"
                type={type}
                handleInputChange={handleInputChange}
                inputs={inputs}
                errors={errors}
                editAccess={true}
              />
            </div>
            <label className="checkbox is-flex pt-4">
                        <input type="checkbox" onClick={handleToggle} />
                        <span className="label"> &nbsp; Show Password</span>
                      </label>
            {/* /.login-card__body */}
            <div className="login-card__footer has-text-centered ">
              <button className="button is-primary is-fullwidth" type="submit">
                Sign In
              </button>
            </div>
            {/* /.login-card__footer */}
          </form>
        </div>
        {/* /.login-card */}
      </div>
      {/* /.login__body */}

      <div className="login__footer">
        <h2 className=" has-text-centered has-text-white">© VAA Technologies Pvt Ltd {currentYear}</h2>
      </div>
      {/* /.login__footer */}
      {loaderContainer}
      {/* <LoaderContainer loading={loading} /> */}
    </div>
  );
};
