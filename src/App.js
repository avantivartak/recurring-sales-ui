import React from "react";
import { ToastContainer } from "react-toastify";
import {BrowserRouter as Router, Route, Navigate, Routes } from "react-router-dom";
import "./App.scss";
import { Admin } from "views/admin/admin";
import { Login } from "views/login";
import { AlertModalProvider } from "features/errorDisplay/alertModalProvider";
import { Sales } from "views/sales/sales";

function App() {
  return (
    <React.Fragment>
      <AlertModalProvider>
      <ToastContainer />
      <Router>
        <Routes>
        <Route path="sales/*" element={<Sales/>} />
        <Route path="admin/*" element={<Admin/>} />
        <Route path="login" element={<Login />} />
        <Route path="/" element={<Navigate to="login"/>}/>
        </Routes>
      </Router>
      </AlertModalProvider>
    </React.Fragment>
  );
}

export default App;
