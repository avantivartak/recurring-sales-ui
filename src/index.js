import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import axios from "axios";
import { globalConfig, globalConfigUrl } from "./core/configuration/config";
import _ from "lodash";
import Moment from "moment";
import momentLocalizer from "react-widgets-moment";
Moment.locale("en");
momentLocalizer(Moment);

const root = ReactDOM.createRoot(document.getElementById("root"));
axios
  .get(globalConfigUrl)
  .then((response) => {
    const hostName=window.location.hostname;
    const configData = response.data
    const { data } = response;
    configData["apiEndPoint"]=_.replace(
      configData["apiEndPoint"],
      "localhost",
      hostName
    )
    configData["wsEndPoint"]=_.replace(
      configData["wsEndPoint"],
      "localhost",
      hostName
    )
    globalConfig.set(configData);
    // console.debug("Global config fetched: ", data);
    return (
      <React.StrictMode>
    
          <App />
      </React.StrictMode>
    );
  })
  .catch((e) => {
    console.log(globalConfigUrl);
    return (
      <p style={{ color: "red", textAlign: "center" }}>
        Error while fetching global config
      </p>
    );
  })
  .then((reactElement) => {
    root.render(reactElement);
  });

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
